package extraction;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import annotation.annotation;

public class extractor {
	static Hashtable partTuples = new Hashtable<>();
	
	// Creates a hashtable with records as keys and the part tuples that occur in that record as values (allowing duplicates)
	private static Hashtable extractPart () throws IOException {
		Hashtable table = new Hashtable<>();
		ArrayList<String> partsFound;
		Document xmlDoc = annotation.getDocument("files/annotatedDataSet.xml");
		NodeList listOfRecords = xmlDoc.getElementsByTagName("record");
		
		// For each record
		for (int i=0 ; i<listOfRecords.getLength() ; i++) {
			partsFound = new ArrayList<String>();

			Node recordNode = listOfRecords.item(i);
			Element recordElement = (Element) recordNode;
			NodeList phraseList = recordElement.getElementsByTagName("phrase");
			
			if (phraseList.getLength() > 0) {
				// For each phrase
				for (int j=0 ; j<phraseList.getLength() ; j ++) {
					NodeList list = phraseList.item(j).getChildNodes();
					
					for (int k=0 ; k < list.getLength() ; k++) {
						// Collect the parts that occur in the phrase
						if (list.item(k).getNodeName() == "part") {	partsFound.add(list.item(k).getTextContent()); }
			}}}

			// Add parts to hashtable
			table.put( (i+1), partsFound);
		}
		
		return table;
	}
	
	// Creates a hashtable with records as keys and the part-symptom tuples that occur in that record as values (allowing duplicates)
	public static Hashtable extractPartSymptom () throws IOException {
		Hashtable partSymptoms = new Hashtable<>();
		
		ArrayList<String> partsFound;
		ArrayList<String> symptomsFound;
		ArrayList<String> combos;
		
		Document xmlDoc = annotation.getDocument("files/annotatedDataSet.xml");
		NodeList listOfRecords = xmlDoc.getElementsByTagName("record");

		// For each record
		for (int i=0 ; i<listOfRecords.getLength() ; i++) {
			combos = new ArrayList<String>();
			
			Node recordNode = listOfRecords.item(i);
			Element recordElement = (Element) recordNode;
			NodeList phraseList = recordElement.getElementsByTagName("phrase");
			
			if (phraseList.getLength() > 0) {
				// For each phrase
				for (int j=0 ; j<phraseList.getLength() ; j ++) {
					partsFound = new ArrayList<String>();
					symptomsFound = new ArrayList<String>();
					
					NodeList list = phraseList.item(j).getChildNodes();
					
					// Collect the parts and symptoms that occur in the phrase
					for (int k=0 ; k < list.getLength() ; k++) {
						if 		(list.item(k).getNodeName() == "part") 		{ partsFound.add(list.item(k).getTextContent())    	; }
						else if (list.item(k).getNodeName() == "symptom") 	{ symptomsFound.add(list.item(k).getTextContent())	; }
					}
					
					// If parts and symptoms co-occur, collect their combination
					if (partsFound.size()>0 && symptomsFound.size()>0) {
						for (String part : partsFound) {
							for (String symptom : symptomsFound) { 
								combos.add(part + "#" + symptom);
							}				
						}
					}
					else if (partsFound.size() == 0 && symptomsFound.size()>0) {
						for (String symptom : symptomsFound) {
							combos.add("empty#" + symptom);
						}
					}
				}
			}
			
			// Add part-symptom combinations to hashtable
			partSymptoms.put(i+1, combos);
		}
		
		return partSymptoms;
	}
	
	private static Hashtable extractPartSymptomFault() throws IOException {
		Hashtable partSymptomFaults = new Hashtable();
		Hashtable faults = new Hashtable();
		Hashtable partSymptoms = extractPartSymptom();

		ArrayList<String> symptoms;
		ArrayList<String> f;
		
		ArrayList<String> faultsFound;
		ArrayList<String> combos;
		
		Document xmlDoc = annotation.getDocument("files/annotatedDataSet.xml");
		NodeList listOfRecords = xmlDoc.getElementsByTagName("record");
		
		// For each record, find the faults
		for (int i=0 ; i<listOfRecords.getLength() ; i++) {
			faultsFound = new ArrayList<String>();

			Node recordNode = listOfRecords.item(i);
			Element recordElement = (Element) recordNode;
			NodeList faultList = recordElement.getElementsByTagName("fault");
			
			if (faultList.getLength() > 0) {
				for (int j=0 ; j<faultList.getLength() ; j++) {
					faultsFound.add(faultList.item(j).getTextContent());
			}}
			faults.put( (i+1), faultsFound);
		}
		
		// For each part-symptom combo in each record, combine with the found faults
		for (Object o : partSymptoms.keySet()) {
			combos = new ArrayList<String>();
			symptoms = (ArrayList<String>) partSymptoms.get(o);
			f = (ArrayList<String>) faults.get(o);
			
			for (String symptom : symptoms) {
				if (symptom.length() > 0) {
					for (String fault : f) {
						combos.add(symptom + "#" + fault);
			}}}

			partSymptomFaults.put(o, combos);
		}
		
		return partSymptomFaults;
	}
	
	private static Hashtable extractFaults() throws IOException {
		Hashtable faults = new Hashtable();
		ArrayList<String> faultsFound;
		
		Document xmlDoc = annotation.getDocument("files/annotatedDataSet.xml");
		NodeList listOfRecords = xmlDoc.getElementsByTagName("record");
		
		// For each record
		for (int i=0 ; i<listOfRecords.getLength() ; i++) {
			// Collect the faults
			faultsFound = new ArrayList<String>();
			Node recordNode = listOfRecords.item(i);
			Element recordElement = (Element) recordNode;
			NodeList faultList = recordElement.getElementsByTagName("fault");
			
			if (faultList.getLength() > 0) {
				for (int j=0 ; j<faultList.getLength() ; j++) {
					faultsFound.add(faultList.item(j).getTextContent());
			}}
			
			faults.put(i+1, faultsFound);
		}

		return faults;
	}	
	
	private static Hashtable extractFaultAction() throws IOException {
		Hashtable faultActions = new Hashtable();
		
		ArrayList<String> faultsFound;
		ArrayList<String> actionsFound;
		ArrayList<String> combos;
		
		Document xmlDoc = annotation.getDocument("files/annotatedDataSet.xml");
		NodeList listOfRecords = xmlDoc.getElementsByTagName("record");
		
		// For each record
		for (int i=0 ; i<listOfRecords.getLength() ; i++) {
			// Collect the faults
			faultsFound = new ArrayList<String>();
			Node recordNode = listOfRecords.item(i);
			Element recordElement = (Element) recordNode;
			NodeList faultList = recordElement.getElementsByTagName("fault");
			
			if (faultList.getLength() > 0) {
				for (int j=0 ; j<faultList.getLength() ; j++) {
					faultsFound.add(faultList.item(j).getTextContent());
			}}
			
			// Collect the actions
			actionsFound = new ArrayList<String>();
			recordNode = listOfRecords.item(i);
			recordElement = (Element) recordNode;
			NodeList actionList = recordElement.getElementsByTagName("action");
			
			if (actionList.getLength() > 0) {
				for (int j=0 ; j<actionList.getLength() ; j++) {
					actionsFound.add(actionList.item(j).getTextContent());
			}}
			
			// Record their combinations
			combos = new ArrayList<String>();
			
			for (String fault : faultsFound) {
				for (String action : actionsFound) {
					combos.add(fault + "#" + action);
			}}
			faultActions.put(i+1, combos);
		}

		return faultActions;
	}
	
	
	
	private static Hashtable extractFaultLocation() throws IOException {
		Hashtable faultLocations = new Hashtable();
		
		ArrayList<String> faultsFound;
		ArrayList<String> locationsFound;
		ArrayList<String> combos;
		
		Document xmlDoc = annotation.getDocument("files/annotatedDataSet.xml");
		NodeList listOfRecords = xmlDoc.getElementsByTagName("record");
		
		// For each record
		for (int i=0 ; i<listOfRecords.getLength() ; i++) {
			// Collect the faults
			faultsFound = new ArrayList<String>();
			Node recordNode = listOfRecords.item(i);
			Element recordElement = (Element) recordNode;
			NodeList faultList = recordElement.getElementsByTagName("fault");
			
			if (faultList.getLength() > 0) {
				for (int j=0 ; j<faultList.getLength() ; j++) {
					faultsFound.add(faultList.item(j).getTextContent());
			}}
			
			// Collect the actions
			locationsFound = new ArrayList<String>();
			recordNode = listOfRecords.item(i);
			recordElement = (Element) recordNode;
			NodeList locationList = recordElement.getElementsByTagName("location");
			
			if (locationList.getLength() > 0) {
				for (int j=0 ; j<locationList.getLength() ; j++) {
					locationsFound.add(locationList.item(j).getTextContent());
			}}
			
			// Record their combinations
			combos = new ArrayList<String>();
			
			for (String fault : faultsFound) {
				for (String location : locationsFound) {
					combos.add(fault + "#" + location);
			}}
			faultLocations.put(i+1, combos);
		}

		return faultLocations;
	}
	
	
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// Dictionary table shows the occurences of parts in the records.
	// This function takes the dictionary as input and creates the partTuples file.
	// (which contains the record, parts that exist in it, as well as their frequency)
	private static void makePartTuples() throws IOException {
		int frequency;
		ArrayList<String> value;
		ArrayList<String> found;

		Hashtable table = extractPart();
		BufferedWriter writer = new BufferedWriter (new FileWriter("files/partTuples"));

		// Get all keys from table dictionary
		Enumeration keys = table.keys();
			
		while (keys.hasMoreElements()) {
			found = new ArrayList<String>();
			
			// Get and write the record number
			Object o = keys.nextElement();
			writer.write(o + ",");

			// Find what parts exist in the record
			value = (ArrayList<String>) table.get(o);
			for (String s : value) { if (! found.contains(s)) { found.add(s); } }
			
			// Write each part and its frequency in the record to the file 
			for (String t : found) {
				frequency = Collections.frequency(value, t);
				writer.write( t + ";" + frequency + "," );
			}
			writer.write("\n");
		}
		
		writer.close();
	}
	
	private static void makePartSymptomTuples() throws IOException {
		int frequency;
		ArrayList<String> value;
		ArrayList<String> found;

		Hashtable table = extractPartSymptom();
		BufferedWriter writer = new BufferedWriter (new FileWriter("files/partSymptomTuples"));

		// Get all keys from table dictionary
		Enumeration keys = table.keys();
			
		while (keys.hasMoreElements()) {
			found = new ArrayList<String>();
			
			// Get and write the record number
			Object o = keys.nextElement();
			writer.write(o + ",");

			// Find what parts exist in the record
			value = (ArrayList<String>) table.get(o);
			for (String s : value) { if (! found.contains(s)) { found.add(s); } }
			
			// Write each part and its frequency in the record to the file 
			for (String t : found) {
				frequency = Collections.frequency(value, t);
				writer.write( t + ";" + frequency + "," );
			}
			writer.write("\n");
		}
		
		writer.close();
	}
	
	
	private static void makePartSymptomFaultTuples() throws IOException {
		int frequency;
		ArrayList<String> value;
		ArrayList<String> found;

		Hashtable table = extractPartSymptomFault();
		BufferedWriter writer = new BufferedWriter (new FileWriter("files/partSymptomFaultTuples"));

		// Get all keys from table dictionary
		Enumeration keys = table.keys();
			
		while (keys.hasMoreElements()) {
			found = new ArrayList<String>();
			
			// Get and write the record number
			Object o = keys.nextElement();
			writer.write(o + ",");

			// Find what parts exist in the record
			value = (ArrayList<String>) table.get(o);
			for (String s : value) { if (! found.contains(s)) { found.add(s); } }
			
			// Write each part and its frequency in the record to the file 
			for (String t : found) {
				frequency = Collections.frequency(value, t);
				writer.write( t + ";" + frequency + "," );
			}
			writer.write("\n");
		}
		
		writer.close();
	}
	
	private static void makeFaultTuples() throws IOException {
		int frequency;
		ArrayList<String> value;
		ArrayList<String> found;

		Hashtable table = extractFaults();
		BufferedWriter writer = new BufferedWriter (new FileWriter("files/faultTuples"));

		// Get all keys from table dictionary
		Enumeration keys = table.keys();
			
		while (keys.hasMoreElements()) {
			found = new ArrayList<String>();
			
			// Get and write the record number
			Object o = keys.nextElement();
			writer.write(o + ",");

			// Find what tuples exist in the record
			value = (ArrayList<String>) table.get(o);
			for (String s : value) { if (! found.contains(s)) { found.add(s); } }
			
			// Write each part and its frequency in the record to the file 
			for (String t : found) {
				frequency = Collections.frequency(value, t);
				writer.write( t + ";" + frequency + "," );
			}
			writer.write("\n");
		}
		
		writer.close();
	}
	
	
	private static void makeFaultActionTuples() throws IOException {
		int frequency;
		ArrayList<String> value;
		ArrayList<String> found;

		Hashtable table = extractFaultAction();
		BufferedWriter writer = new BufferedWriter (new FileWriter("files/faultActionTuples"));

		// Get all keys from table dictionary
		Enumeration keys = table.keys();
			
		while (keys.hasMoreElements()) {
			found = new ArrayList<String>();
			
			// Get and write the record number
			Object o = keys.nextElement();
			writer.write(o + ",");

			// Find what tuples exist in the record
			value = (ArrayList<String>) table.get(o);
			for (String s : value) { if (! found.contains(s)) { found.add(s); } }
			
			// Write each part and its frequency in the record to the file 
			for (String t : found) {
				frequency = Collections.frequency(value, t);
				writer.write( t + ";" + frequency + "," );
			}
			writer.write("\n");
		}
		
		writer.close();
	}
	
	
	private static void makeFaultLocationTuples() throws IOException {
		int frequency;
		ArrayList<String> value;
		ArrayList<String> found;

		Hashtable table = extractFaultLocation();
		BufferedWriter writer = new BufferedWriter (new FileWriter("files/faultLocationTuples"));

		// Get all keys from table dictionary
		Enumeration keys = table.keys();
			
		while (keys.hasMoreElements()) {
			found = new ArrayList<String>();
			
			// Get and write the record number
			Object o = keys.nextElement();
			writer.write(o + ",");

			// Find what tuples exist in the record
			value = (ArrayList<String>) table.get(o);
			for (String s : value) { if (! found.contains(s)) { found.add(s); } }
			
			// Write each part and its frequency in the record to the file 
			for (String t : found) {
				frequency = Collections.frequency(value, t);
				writer.write( t + ";" + frequency + "," );
			}
			writer.write("\n");
		}
		
		writer.close();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// This function calculates the criticality of parts for the fault diagnosis process.
	private static void calculateCriticalParts() throws IOException {
		String record;
		String part;
		String[] line;
		
		int occurences;										// how many times a tuple has occured in the record
		int allOccurences;									// how many tuples occur in the record
		int numberOfRecords = getNumberOfRecords();			// how many records there are
		int numberOfRecordsForTuple;						// in how many records the tuple occurs
				
		double frequency;			// tuple frequency in the record
		double importance;			// tuple importance in the dataset
		double criticality;
		
		BufferedWriter writer = new BufferedWriter ( new FileWriter("files/partCriticality"));
				
		BufferedReader reader = new BufferedReader ( new FileReader("files/partTuples") );
		Scanner scanner = new Scanner (reader);
		
		// For each record
		while (scanner.hasNextLine()) {
			record = scanner.nextLine();
			line = record.split(",");
			
			writer.write(line[0] + ",");
			
			for (int i=1 ; i<line.length ; i++) {
				part = line[i].split(";")[0];
				occurences = Integer.parseInt(line[i].split(";")[1]);
				allOccurences = getSumOfRecordOccurences(record);
				numberOfRecordsForTuple = getNumberOfRecordsForTuple(part, "files/partTuples");
				
				frequency = occurences / (allOccurences * 1.0) ;
				// Uncomment the following two lines for tf-idf
				importance = Math.log( numberOfRecords ) / numberOfRecordsForTuple ;
				criticality = frequency * importance;
				// Uncomment the following line for simple term frequency
//				criticality = frequency;
				
				writer.write(part + ";" + criticality + ",");
			}
			writer.write("\n");
		}
		
		scanner.close();
		writer.close();
	}
	
	private static Hashtable calculateCriticalPartSymptom() throws IOException {
		String record;
		String tuple;
		String value;
		String[] line;
		
		int occurences;										// how many times a tuple has occured in the record
		int allOccurences;									// how many tuples occur in the record
		int numberOfRecords = getNumberOfRecords();			// how many records there are
		int numberOfRecordsForTuple;						// in how many records the tuple occurs
				
		double frequency;			// tuple frequency in the record
		double importance;			// tuple importance in the dataset
		double criticality;
		
		Hashtable partSymptomCriticality = new Hashtable();
				
		BufferedReader reader = new BufferedReader ( new FileReader("files/partSymptomTuples") );
		Scanner scanner = new Scanner (reader);
		
		while (scanner.hasNextLine()) {
			record = scanner.nextLine();
			line = record.split(",");
			value = "";
			
			for (int i=1 ; i<line.length ; i++) {
				tuple = line[i].split(";")[0];
				occurences = Integer.parseInt(line[i].split(";")[1]);
				allOccurences = getSumOfRecordOccurences(record);
				numberOfRecordsForTuple = getNumberOfRecordsForTuple(tuple, "files/partSymptomTuples");
				
				frequency = occurences / (allOccurences * 1.0) ;
				// Uncomment the following two lines for tf-idf
				importance = Math.log( numberOfRecords ) / numberOfRecordsForTuple ;
				criticality = frequency * importance;
				// Uncomment the following line for simple term frequency
//				criticality = frequency;
				
				value += tuple + ";" + criticality + ",";
			}
			partSymptomCriticality.put(line[0], value);
		}
		
		scanner.close();
		return partSymptomCriticality;
	}
	
	private static Hashtable calculateCriticalPartSymptomFault() throws IOException {
		String record;
		String tuple;
		String value;
		String[] line;
		
		int occurences;										// how many times a tuple has occured in the record
		int allOccurences;									// how many tuples occur in the record
		int numberOfRecords = getNumberOfRecords();			// how many records there are
		int numberOfRecordsForTuple;						// in how many records the tuple occurs
				
		double frequency;			// tuple frequency in the record
		double importance;			// tuple importance in the dataset
		double criticality;
		
		Hashtable partSymptomFaultCriticality = new Hashtable();
				
		BufferedReader reader = new BufferedReader ( new FileReader("files/partSymptomFaultTuples") );
		Scanner scanner = new Scanner (reader);
		
		while (scanner.hasNextLine()) {
			record = scanner.nextLine();
			line = record.split(",");
			value = "";
			
			for (int i=1 ; i<line.length ; i++) {
				tuple = line[i].split(";")[0];
				occurences = Integer.parseInt(line[i].split(";")[1]);
				allOccurences = getSumOfRecordOccurences(record);
				numberOfRecordsForTuple = getNumberOfRecordsForTuple(tuple, "files/partSymptomFaultTuples");
				
				frequency = occurences / (allOccurences * 1.0) ;
				// Uncomment the following two lines for tf-idf
				importance = Math.log( numberOfRecords ) / numberOfRecordsForTuple ;
				criticality = frequency * importance;
				// Uncomment the following line for simple term frequency
//				criticality = frequency;
				
				value += tuple + ";" + criticality + ",";
			}
			partSymptomFaultCriticality.put(line[0], value);
		}
		
		scanner.close();
		return partSymptomFaultCriticality;
	}
	
	private static Hashtable calculateCriticalFaults() throws IOException {
		String record;
		String tuple;
		String value;
		String[] line;
		
		int occurences;										// how many times a tuple has occured in the record
		int allOccurences;									// how many tuples occur in the record
		int numberOfRecords = getNumberOfRecords();			// how many records there are
		int numberOfRecordsForTuple;						// in how many records the tuple occurs
				
		double frequency;			// tuple frequency in the record
		double importance;			// tuple importance in the dataset
		double criticality;
		
		Hashtable faultCriticality = new Hashtable();
				
		BufferedReader reader = new BufferedReader ( new FileReader("files/faultTuples") );
		Scanner scanner = new Scanner (reader);
		
		while (scanner.hasNextLine()) {
			record = scanner.nextLine();
			line = record.split(",");
			value = "";
			
			for (int i=1 ; i<line.length ; i++) {
				tuple = line[i].split(";")[0];
				occurences = Integer.parseInt(line[i].split(";")[1]);
				allOccurences = getSumOfRecordOccurences(record);
				numberOfRecordsForTuple = getNumberOfRecordsForTuple(tuple, "files/faultTuples");
				
				frequency = occurences / (allOccurences * 1.0) ;
				// Uncomment the following two lines for tf-idf
				importance = Math.log( numberOfRecords ) / numberOfRecordsForTuple ;
				criticality = frequency * importance;
				// Uncomment the following two lines for simple term frequency
//				criticality = frequency;
				
				value += tuple + ";" + criticality + ",";
			}
			faultCriticality.put(line[0], value);
		}
		
		scanner.close();
		return faultCriticality;
	}
	
	private static Hashtable calculateCriticalFaultAction() throws IOException {
		String record;
		String tuple;
		String value;
		String[] line;
		
		int occurences;										// how many times a tuple has occured in the record
		int allOccurences;									// how many tuples occur in the record
		int numberOfRecords = getNumberOfRecords();			// how many records there are
		int numberOfRecordsForTuple;						// in how many records the tuple occurs
				
		double frequency;			// tuple frequency in the record
		double importance;			// tuple importance in the dataset
		double criticality;
		
		Hashtable faultActionCriticality = new Hashtable();
				
		BufferedReader reader = new BufferedReader ( new FileReader("files/faultActionTuples") );
		Scanner scanner = new Scanner (reader);
		
		while (scanner.hasNextLine()) {
			record = scanner.nextLine();
			line = record.split(",");
			value = "";
			
			for (int i=1 ; i<line.length ; i++) {
				tuple = line[i].split(";")[0];
				occurences = Integer.parseInt(line[i].split(";")[1]);
				allOccurences = getSumOfRecordOccurences(record);
				numberOfRecordsForTuple = getNumberOfRecordsForTuple(tuple, "files/faultActionTuples");
				
				frequency = occurences / (allOccurences * 1.0) ;
				// Uncomment the following two lines for tf-idf
				importance = Math.log( numberOfRecords ) / numberOfRecordsForTuple ;
				criticality = frequency * importance;
				// Uncomment the following line for simple term frequency
//				criticality = frequency;
				
				value += tuple + ";" + criticality + ",";
			}
			faultActionCriticality.put(line[0], value);
		}
		
		scanner.close();
		return faultActionCriticality;
	}
	
	private static Hashtable calculateCriticalFaultLocation() throws IOException {
		String record;
		String tuple;
		String value;
		String[] line;
		
		int occurences;										// how many times a tuple has occured in the record
		int allOccurences;									// how many tuples occur in the record
		int numberOfRecords = getNumberOfRecords();			// how many records there are
		int numberOfRecordsForTuple;						// in how many records the tuple occurs
				
		double frequency;			// tuple frequency in the record
		double importance;			// tuple importance in the dataset
		double criticality;
		
		Hashtable faultLocationCriticality = new Hashtable();
				
		BufferedReader reader = new BufferedReader ( new FileReader("files/faultLocationTuples") );
		Scanner scanner = new Scanner (reader);
		
		while (scanner.hasNextLine()) {
			record = scanner.nextLine();
			line = record.split(",");
			value = "";
			
			for (int i=1 ; i<line.length ; i++) {
				tuple = line[i].split(";")[0];
				occurences = Integer.parseInt(line[i].split(";")[1]);
				allOccurences = getSumOfRecordOccurences(record);
				numberOfRecordsForTuple = getNumberOfRecordsForTuple(tuple, "files/faultLocationTuples");
				
				frequency = occurences / (allOccurences * 1.0) ;
				// Uncomment the following two lines for tf-idf
				importance = Math.log( numberOfRecords ) / numberOfRecordsForTuple ;
				criticality = frequency * importance;
				// Uncomment the following line for simple term frequency
//				criticality = frequency;
				
				value += tuple + ";" + criticality + ",";
			}
			faultLocationCriticality.put(line[0], value);
		}
		
		scanner.close();
		return faultLocationCriticality;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static Hashtable<String, String> getPartSymptomCriticality() throws IOException {
		Hashtable <String, String> table = calculateCriticalPartSymptom();
		return table;
	}
	
	public static Hashtable<String, String> getPartSymptomFaultCriticality() throws IOException {
		Hashtable <String, String> table = calculateCriticalPartSymptomFault();
		return table;
	}
	
	public static Hashtable<String, String> getFaultCriticality() throws IOException {
		Hashtable <String, String> table = calculateCriticalFaults();
		return table;
	}
	
	public static Hashtable<String, String> getFaultActionCriticality() throws IOException {
		Hashtable <String, String> table = calculateCriticalFaultAction();
		return table;
	}
	
	public static Hashtable<String, String> getFaultLocationCriticality() throws IOException {
		Hashtable <String, String> table = calculateCriticalFaultLocation();
		return table;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// This function counts the number of all tuples that exist in a record.
	private static int getSumOfRecordOccurences(String record) {
		int number = 0;
		String[] line = record.split(","); 
		for (int i=1 ; i<line.length ; i++) {  number += Integer.parseInt(line[i].split(";")[1]);  }
		return number;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// This function returns the total number of records in the dataset (used to calculate criticality)
	private static int getNumberOfRecords() throws FileNotFoundException {
		int number = 0;
		BufferedReader reader = new BufferedReader( new FileReader("files/partTuples") );
		Scanner scanner = new Scanner(reader);
		while (scanner.hasNextLine()) { scanner.nextLine() ;  number++ ; }
		scanner.close();
		return number;
	}
	
	// This function takes as input a tuple and returns a number indicating in how many records it appears in.
	private static int getNumberOfRecordsForTuple (String tuple, String file) throws FileNotFoundException {
		int number = 0;
		String[] line;
		
		BufferedReader reader = new BufferedReader( new FileReader(file) );
		Scanner scanner = new Scanner(reader);
		
		while (scanner.hasNextLine()) {
			line = scanner.nextLine().split(",");
			for (int i=1 ; i<line.length ; i++) { if (line[i].split(";")[0].equals(tuple)) { number++ ; } }
		}
		
		scanner.close();
		return number;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void extract() throws IOException {
		makePartTuples();
		makePartSymptomTuples();
		makePartSymptomFaultTuples();
		makeFaultTuples();
		makeFaultActionTuples();
		makeFaultLocationTuples();
		
		calculateCriticalParts();
		calculateCriticalPartSymptom();
		calculateCriticalPartSymptomFault();
		calculateCriticalFaults();
		calculateCriticalFaultAction();
		calculateCriticalFaultLocation();
	}
	
	
	
	public static void main (String[] args) throws IOException {}
}
