package gui;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;

import run.createKnowledge;

import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

public class gui {
	
	static JTextArea textArea;
	static JList list;
	static JList list_two;
	static JTextArea ta;
	static DefaultListModel faultsModel;
	static DefaultListModel model_1;
	
	private gui() {
		setupGui() ;
	}
	
	public void setupGui() {
		final JFrame window = new JFrame("Fault finder");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		window.setContentPane(panel);
		
		JButton btnBuildData = new JButton("Re-build Data");
		btnBuildData.setBounds(423, 56, 111, 23);
		btnBuildData.setToolTipText("Clusters the whole dataset again");
		btnBuildData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					createKnowledge.create();
					createKnowledge.buildData();
				} 
				catch (OWLOntologyCreationException e) { e.printStackTrace(); }
				catch (IOException e) { e.printStackTrace(); }
			}
		});
		panel.setLayout(null);
		panel.add(btnBuildData);
		
		JButton btnAddRecord = new JButton("Add record");
		btnAddRecord.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addRecord add = new addRecord();
			}
		});
		btnAddRecord.setBounds(423, 90, 111, 23);
		btnAddRecord.setToolTipText("Re-build the data or restart the application for the record to be taken into account.");
		panel.add(btnAddRecord);
		
		JButton btnNewButton = new JButton("Search");
		btnNewButton.setBounds(314, 334, 89, 23);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String text = textArea.getText();

				if (textArea.getText().trim().length() == 0) {
					JOptionPane.showMessageDialog(window, "Please enter some text!", "Invalid text", JOptionPane.PLAIN_MESSAGE);
				}
				else {
					result res = new result();
				}
			}
		});
		panel.add(btnNewButton);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(32, 26, 371, 297);
		panel.add(tabbedPane);
		
		JScrollPane scrollPane = new JScrollPane();
		tabbedPane.addTab("Enter text", null, scrollPane, null);
		
		textArea = new JTextArea("Describe the symptoms of the train you are trying to repair.");
		textArea.setWrapStyleWord(true);
		scrollPane.setViewportView(textArea);
		textArea.setLineWrap(true);
		
		model_1 = new DefaultListModel();
		
		// Confirm closing
		window.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(WindowEvent windowEvent) {
		    	int j = JOptionPane.showConfirmDialog(window, "Are you sure to close this window?", "Really Closing?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		    	if (j == 0) { System.exit(0); }
		    }
		});
		
		window.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		window.setSize(570,509);
		window.setVisible(true);
	}
	
	class result extends JFrame {
		public result() {
			
			try {
				// Symptoms found
				Hashtable partSymptoms = createKnowledge.mineText(textArea.getText());
				if (partSymptoms.keySet().size() == 0) {
					JOptionPane.showMessageDialog(null, "Sorry, no symptoms were identified in the provided text.");
				}
				else {
					JFrame win = new JFrame("Result");
					win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					
					JPanel pan = new JPanel();
					win.setContentPane(pan);
					pan.setLayout(null);
					
					JLabel symptomsLabel_one = new JLabel("Symptoms found");
					symptomsLabel_one.setBounds(20,10,200,20);
					pan.add(symptomsLabel_one);
					
					JLabel symptomsLabel_two = new JLabel("(and their significance in the text)");
					symptomsLabel_two.setBounds(20,25,200,20);
					pan.add(symptomsLabel_two);
					
					DefaultListModel model = new DefaultListModel();
					for (Object o : partSymptoms.keySet()) {
						String part = ((String) o).split("#")[0];
						String symptom = ((String) o).split("#")[1];
						model.addElement("Part: " + part + ",  Symptom: " + symptom + ",    " + partSymptoms.get(o) + " %");
					}
					list = new JList(model);
					list.setBounds(20, 50, 380, 200);
					list.setBorder(BorderFactory.createLineBorder(Color.black));
					pan.add(list);
					
					JLabel symptomsLabel_three = new JLabel("Select a symptom to see its probable causes.");
					symptomsLabel_three.setBounds(20,250,350,20);
					pan.add(symptomsLabel_three);
					
					// Probable faults
					JLabel faultsLabel = new JLabel("Probable faults");
					faultsLabel.setBounds(462,25,200,20);
					pan.add(faultsLabel);
					
					faultsModel = new DefaultListModel();
					ListSelectionListener currentSymptom = new ListSelectionListener() {
		            	@Override
		            	public void valueChanged (ListSelectionEvent arg0) {
		            		if (!list.getValueIsAdjusting()) {
		            			String symptom = (String) list.getSelectedValue() ;
		                		if (symptom != null) { 
			               			faultsModel.clear() ;
			               			String ps = symptom.split(",")[0].substring(6) + "#" + symptom.split(",")[1].substring(11);
			               			ArrayList<String> faults = createKnowledge.findFault(ps);
			                   		for ( int i=0 ; i<faults.size(); i++ ) {
		                    			faultsModel.addElement(faults.get(i)) ;
		                    		}
		               			}
		            		}
		            	}
		            };
		            list_two = new JList(faultsModel);
		            list.addListSelectionListener(currentSymptom);
		            list_two.setBorder(BorderFactory.createLineBorder(Color.black));
					list_two.setBounds(460, 50, 400, 200);
					pan.add(list_two);
					
					JLabel faultsLabel_two = new JLabel("Select a fault to see its probable location and recommended actions.");
					faultsLabel_two.setBounds(462,250,450,20);
					pan.add(faultsLabel_two);
					
					
					// Location and actions
					JLabel actionsLabel = new JLabel("Fault Location & Actions");
					actionsLabel.setBounds(900,25,350,20);
					pan.add(actionsLabel);
					
					ta = new JTextArea();
					ta.setBorder(BorderFactory.createLineBorder(Color.black));
					ta.setBounds(900,50,350,200);
					pan.add(ta);
					
					ListSelectionListener currentFault = new ListSelectionListener() {
		            	@Override
		            	public void valueChanged (ListSelectionEvent arg0) {
		            		if (!list_two.getValueIsAdjusting()) {
		            			Object f = list_two.getSelectedValue();
		            			if (f != null) {
		            				String fault = ((String) f).split(" ")[0] ;
			                		if (fault != null) { 
			                			String r = "";
			                			
			                			if (createKnowledge.findLocation(fault) != null) {
			                				ArrayList<String> locations = createKnowledge.findLocation(fault);
			                				r += " Probable locations of the fault:\n";
			    	               			for (String l : locations) { r += "    " + l + "\n"; }
			                			}
			                			
			                			if (createKnowledge.findAction(fault) != null) {
			                				ArrayList<String> actions = createKnowledge.findAction(fault);
			                				r += "\n\n Recommended actions for the fault:\n";
			    	               			for (String a : actions) { r += "    " + a + "\n"; }
			                			}
				               			
				               			ta.setText(r);
			               			}
		            			}
		            		}
		            	}
		            };
					list_two.addListSelectionListener(currentFault);
					
					JLabel note = new JLabel("* If there is no specific part associated with the symptom, then the part field value is 'empty'.");
					note.setBounds(390,330,600,20);
					pan.add(note);

					win.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
					win.setSize(1300,400);
					win.setVisible(true);
				}

				
			}
			catch (OWLOntologyCreationException e) { e.printStackTrace();}
			catch (IOException e) { e.printStackTrace();}
		}
	}
	
	class addRecord extends JFrame {
		JFrame window;
		JTextArea description, analysis, action;
		JComboBox<String> fl, fault;
		
		public addRecord() {
			window = new JFrame("Add record");
			JPanel panel  = new JPanel();
			window.setContentPane(panel);
			window.getContentPane().setLayout(null);
			
			// Description
			JLabel descriptionLabel = new JLabel("Description");
			descriptionLabel.setBounds(20,10,100,20);
			panel.add(descriptionLabel);
			
			JScrollPane dScrollPane = new JScrollPane();
			dScrollPane.setBounds(20,35,400,200);
			panel.add(dScrollPane);
			
			description = new JTextArea("Describe the symptoms of the problem.");
			description.setBorder(BorderFactory.createLineBorder(Color.black));
			description.setLineWrap(true);
			description.setWrapStyleWord(true);
			
			dScrollPane.setViewportView(description);
			
			// Analysis
			JLabel analysisLabel = new JLabel("Analysis");
			analysisLabel.setBounds(450,10,100,20);
			panel.add(analysisLabel);
			
			JScrollPane anScrollPane = new JScrollPane();
			anScrollPane.setBounds(450,35,400,200);
			panel.add(anScrollPane);
			
			analysis = new JTextArea("Write down a preliminary analysis of the problem.");
			analysis.setBorder(BorderFactory.createLineBorder(Color.black));
			analysis.setLineWrap(true);
			analysis.setWrapStyleWord(true);
			
			anScrollPane.setViewportView(analysis);
			
			// Fault Location
			JLabel flLabel = new JLabel("Fault Location");
			flLabel.setBounds(20,260,200,20);
			panel.add(flLabel);
			
			fl = new JComboBox<String>();
			
			fl.setModel(new DefaultComboBoxModel<String>() {
			    boolean selectionAllowed = true;

			    @Override
			    public void setSelectedItem(Object anObject) {
			    	if (!"Select the location of the fault".equals(anObject)) {
			    		super.setSelectedItem(anObject);
			    	} 
			    	else if (selectionAllowed) {
			    		// Allow this just once
			    		selectionAllowed = false;
			    		super.setSelectedItem(anObject);
			    	}
			    }
			});

			fl.addItem("Select the location of the fault");
			Hashtable loc = ontology.ontology.getParts();
			ArrayList<String> locations = new ArrayList<String>();
			for (Object o : loc.keySet()) { locations.add((String) o); }
			Collections.sort(locations);
			for (String location : locations) { fl.addItem(location); }
			
			fl.setBounds(20,280,300,20);
			panel.add(fl);
			
			// Fault
			JLabel faultLabel = new JLabel("Fault");
			faultLabel.setBounds(20,320,200,20);
			panel.add(faultLabel);
			
			fault = new JComboBox<String>();
			
			fault.setModel(new DefaultComboBoxModel<String>() {
			    boolean selectionAllowed = true;

			    @Override
			    public void setSelectedItem(Object anObject) {
			    	if (!"Select the fault".equals(anObject)) {
			    		super.setSelectedItem(anObject);
			    	} 
			    	else if (selectionAllowed) {
			    		// Allow this just once
			    		selectionAllowed = false;
			    		super.setSelectedItem(anObject);
			    	}
			    }
			});

			fault.addItem("Select the fault");
			Hashtable f = ontology.ontology.getFaults();
			ArrayList<String> faults = new ArrayList<String>();
			for (Object o : f.keySet()) { faults.add((String) o); }
			Collections.sort(faults);
			for (String flt : faults) { fault.addItem(flt); }
			
			fault.setBounds(20,340,300,20);
			panel.add(fault);
			
			// Recommended action
			JLabel actionLabel = new JLabel("Recommended Actions");
			actionLabel.setBounds(450,260,200,20);
			panel.add(actionLabel);
			
			JScrollPane acScrollPane = new JScrollPane();
			acScrollPane.setBounds(450,285,400,200);
			panel.add(acScrollPane);
			
			action = new JTextArea("Describe the actions needed for the problem to be solved.");
			action.setBorder(BorderFactory.createLineBorder(Color.black));
			action.setLineWrap(true);
			action.setWrapStyleWord(true);
			
			acScrollPane.setViewportView(action);
			
			// Add button
			JButton button = new JButton("Add record");
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String d = description.getText().trim();
					String an = analysis.getText().trim();
					String l = fl.getSelectedItem().toString().trim();
					String f = fault.getSelectedItem().toString().trim();
					String ac = action.getText().trim();
					
					if (d.equals("Describe the symptoms of the problem.")) { d = ""; }
					if (an.equals("Write down a preliminary analysis of the problem.")) { an = ""; }
					if (l.equals("Select the location of the fault")) { l = ""; }
					if (f.equals("Select the fault")) { f = ""; }
					if (ac.equals("Describe the actions needed for the problem to be solved.")) { ac = ""; }
					
					if (d.equals("") && an.equals("") && l.equals("") && f.equals("") && ac.equals("")) {
						JOptionPane.showMessageDialog(window, "Please provide some information!", "Invalid entry", JOptionPane.PLAIN_MESSAGE);
					}
					else if (d.equals("") || an.equals("") || l.equals("") || f.equals("") || ac.equals("")) {
						JOptionPane.showMessageDialog(window, "Please fill out all fields!", "Invalid entry", JOptionPane.PLAIN_MESSAGE);
					}
					else {
						try {
							createKnowledge.addRecord(d, an, l, f, ac);
							JOptionPane.showMessageDialog(window, "Record successfully added!", "Success", JOptionPane.PLAIN_MESSAGE);
						}
						catch (IOException e) { e.printStackTrace(); }
					}
				}
			});
			button.setBounds(700,510,150,25);
			panel.add(button);
			
			window.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			window.setSize(900,600);
			window.setVisible(true);
		}
	}
	
	public static JTextArea getTextArea() {
		return textArea;
	}
	
	public static void main (String[] args) {
		gui g = new gui() ;
	}
}
