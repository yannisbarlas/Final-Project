package clustering;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import extraction.extractor;
import ontology.ontology;

public class partSymptomBased {
	
	static Hashtable<String, ArrayList<String>> clusters = new Hashtable<String, ArrayList<String>>();
	static Hashtable<String, ArrayList<String>> subClusters = new Hashtable<String, ArrayList<String>>();
	static Hashtable alias = new Hashtable();
	
	// Makes a set of all the part-symptom tuples that are present in the dataset
	public static ArrayList<String> makeSetOfPartSymptom() throws IOException {
		String partSymptom;
		String[] line;
		ArrayList<String> partSymptoms = new ArrayList<String>();
		Hashtable psCriticality = extractor.getPartSymptomCriticality();

		for (Object o : psCriticality.keySet()) {
			line = ((String) psCriticality.get(o)).split(",");
			for (String s : line) {
				partSymptom = s.split(";")[0];
				if (! partSymptoms.contains(partSymptom)) {
					if (partSymptom.length() > 0) {
						partSymptoms.add(partSymptom);
					}
				}
			}
		}
				
		return partSymptoms;
	}
	
	// Makes a cluster for each part-symptom combination identified in the dataset
	public static void makeClusters() throws IOException {
		ArrayList<String> partSymptoms = makeSetOfPartSymptom();
		ArrayList<String> empty = new ArrayList<String>();
		
		for (String partSymptom : partSymptoms) {
			ArrayList<String> arr = (ArrayList<String>) empty.clone();
			clusters.put(partSymptom, arr);
		}
	}
	
	// Clusters the records to a part-symptom subcluster of part 
	private static void cluster() throws IOException, OWLOntologyCreationException {
		double max;
		
		String part;
		String partSymptom;
		String[] line;
		
		ArrayList<String> records;
		ArrayList<String> symptomsOfPart;
		ArrayList<String> assigned;
		
		Hashtable partClusters = partBased.getClusters();
		Hashtable al = partBased.getAlias();
		
		// For each part cluster
		for (Object o : partClusters.keySet()) {
			String temp = "";
			part = (String) o;
			records = (ArrayList<String>) partClusters.get(o);
			
			// For each record in the cluster
			for (String record : records) {
				partSymptom = "";
				max = 0;
				
				// Make sure it's not an empty record
				if (extractor.getPartSymptomCriticality().get(record).length() > 0) {
					line = extractor.getPartSymptomCriticality().get(record).split(",");
					boolean related;
					String p;
					String s;
					
					// Find the symptoms that cooccur with the part and choose the one with the maximum criticality
					for (String item : line) {
						p = item.split("#")[0];
						
						if (! partClusters.keySet().contains(p)) {
							if (al.keySet().contains(p)) {
								p = (String) al.get(p);
							}
						}

						if (p.equals(part)) {
							related = false;
							s = item.split(";")[0].split("#")[1];
							
							// Check that the part and the symptom are actually related
							if (ontology.checkRelation(s, p, "Referto")) { related = true; }
							if ((related || p.equals("empty")) && Double.parseDouble(item.split(";")[1]) > max) {
								partSymptom = item.split(";")[0];
								max = Double.parseDouble(item.split(";")[1]);
							}
						}
						else if (p.equals("empty")) {
							if (Double.parseDouble(item.split(";")[1]) > max) {
								partSymptom = item.split(";")[0];
								max = Double.parseDouble(item.split(";")[1]);
							}
						}
					}
					
					if (partSymptom.split("#")[0].equals("empty")) {temp=part ; part = "empty";}
					if (partSymptom.length() > 0) {
						// If part doesn't exist in subclusters already
						if (subClusters.get(part) == null) {
							// Create part/part-symptom entry in subclusters
							symptomsOfPart = new ArrayList<String>();
							symptomsOfPart.add(partSymptom);
							subClusters.put(part, symptomsOfPart);
							
							// Assign record to part-symptom
							assigned = new ArrayList<String>();
							assigned.add(record);
							clusters.put(partSymptom, assigned);
						}
						// If part already exists in subclusters
						else {
							// If this part-symptom combo already exists for this part
							if (subClusters.get(part).contains(partSymptom)) {
								// Assign record to part-symptom
								assigned = clusters.get(partSymptom);
								assigned.add(record);
								clusters.put(partSymptom, assigned);
							}
							// If this part-symptom combo doesn't exist for this part
							else {
								// Create it
								symptomsOfPart = subClusters.get(part);
								symptomsOfPart.add(partSymptom);
								subClusters.put(part, symptomsOfPart);
								
								// Assign record to part-symptom
								assigned = new ArrayList<String>();
								assigned.add(record);
								clusters.put(partSymptom, assigned);
			
							}
						}
					}
					if (temp.length() > 0) {part = temp;}
				}
			}
		}
		
		// Remove empty clusters
		ArrayList<Object> toRemove = new ArrayList<Object>();
		for (Object o : clusters.keySet()) {
			if (clusters.get(o).size() == 0) {
				toRemove.add(o);
			}
		}
		for (Object obj : toRemove) {
			clusters.remove(obj);
		}
	}
	
	private static void mergeClusters() throws OWLOntologyCreationException {
		ArrayList<String> voidCombos = new ArrayList<String>();
		
		boolean keepLooping = true;
		
		while (keepLooping) {
			keepLooping = false;
			
			String p = "";
			String c1 = "";
			String c2 = "";
			String temp;
			ArrayList<String> toAdd = new ArrayList<String>();
			Hashtable partClusters = partBased.getClusters();
			Hashtable al = partBased.getAlias();

			outerloop:
			// For all cluster combinations
			for (String cluster1 : clusters.keySet()) {
				for (String cluster2 : clusters.keySet()) {
					
					// If they are not the same
					if (! cluster1.equals(cluster2)) {
						// If they belong to the same part super-cluster AND the reverse combination has not already been marked as void
						if (cluster1.split("#")[0].equals(cluster2.split("#")[0]) && !voidCombos.contains(cluster2 + "&" + cluster1)) {
							// Get the symptoms from the part-symptom combinations
							String s1 = cluster1.split("#")[1];
							String s2 = cluster2.split("#")[1];
							
							// If the two symptoms observe the same fault mode
							if (ontology.observeSameFM(s1, s2)) {
								// Clusters to merge identified --- keep looping
								keepLooping = true;

								p = cluster1.split("#")[0];
								if (! partClusters.keySet().contains(p)) {
									if (al.keySet().contains(p)) {
										p = (String) al.get(p);
									}
								}
								c1 = cluster1;
								c2 = cluster2;
								toAdd = clusters.get(cluster2);
								
								break outerloop;
							}
						}
						else {
							// Mark the combination as void if the tests have failed
							voidCombos.add(cluster1 + "&" + cluster2);
						}
					}
				}
			}
			// Merge clusters and adjust
			if (c1.length()> 0 && c2.length()>0 && p.length()>0 && toAdd.size() > 0) {
				ArrayList<String> first = clusters.get(c1);
				for (String record : toAdd) { first.add(record); }
				
				clusters.put(c1, first);
				clusters.remove(c2);
				alias.put(c2, c1);
				
				subClusters.get(p).remove(c2);
			}
		}
	}
	
	public static void run() throws IOException, OWLOntologyCreationException {
		makeClusters();
		cluster();
		mergeClusters();
	}
	
	public static Hashtable getClusters() {
		return clusters;
	}
	
	public static Hashtable getSubClusters() {
		return subClusters;
	}
	
	public static Hashtable getAlias() {
		return alias;
	}
	
	public static void main (String[] args) throws IOException, OWLOntologyCreationException {}
}
