package clustering;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import extraction.extractor;
import ontology.ontology;

public class partSymptomFaultBased {
	
	static Hashtable<String, ArrayList<String>> clusters = new Hashtable<String, ArrayList<String>>();
	static Hashtable<String, ArrayList<String>> subClusters = new Hashtable<String, ArrayList<String>>();
	
	public static ArrayList<String> makeSetOfPartSymptomFault() throws IOException {
		String partSymptomFault;
		String[] line;
		ArrayList<String> partSymptomFaults = new ArrayList<String>();
		
		Hashtable table = extraction.extractor.getPartSymptomFaultCriticality();
		
		for (Object o : table.keySet()) {
			line = ((String) table.get(o)).split(",");
			for (int i=0 ; i<line.length ; i++) {
				if (line[i].length() > 0) {
					partSymptomFault = line[i].split(";")[0];
					if (! partSymptomFaults.contains(partSymptomFault)) { 
						partSymptomFaults.add(partSymptomFault) ; 
					}
		}}}
		
		return partSymptomFaults;
	}
	
	public static void makeClusters() throws IOException {
		ArrayList<String> partSymptomFaults = makeSetOfPartSymptomFault();
		ArrayList<String> empty = new ArrayList<String>();
		
		for (String partSymptomFault : partSymptomFaults) {
			ArrayList<String> arr = (ArrayList<String>) empty.clone();
			clusters.put(partSymptomFault, arr);
		}
	}
	
	private static void cluster() throws IOException, OWLOntologyCreationException {
		double max;
		
		String partSymptom;
		String partSymptomFault;
		String[] line;
		
		ArrayList<String> records;
		ArrayList<String> faultsOfPartSymptom;
		ArrayList<String> partSymptoms;
		ArrayList<String> assigned;
		
		Hashtable partSymptomClusters = partSymptomBased.getClusters();
		Hashtable alias = partSymptomBased.getAlias();
		
		// For each part-symptom cluster
		for (Object o : partSymptomClusters.keySet()) {
			partSymptom = (String) o;
			records = (ArrayList<String>) partSymptomClusters.get(o);
			
			// For each record in the cluster
			for (String record : records) {
				partSymptomFault = "";
				max = 0;
				
				// Make sure it's not an empty record
				if (extractor.getPartSymptomFaultCriticality().get(record).length() > 0) {
					line = extractor.getPartSymptomFaultCriticality().get(record).split(",");
					
					for (String item : line) {
						String p = item.split("#")[0];
						String s = item.split("#")[1];
						String f = item.split("#")[2].split(";")[0];
						
						if (! partSymptomClusters.keySet().contains(p + "#" + s)) {
							if (alias.keySet().contains(p + "#" + s)) {
								String ps = (String) alias.get(p + "#" + s);
								p = ps.split("#")[0];
								s = ps.split("#")[1];
							}
						}
						
						if ((p + "#" + s).equals(partSymptom)) {
							boolean related = false;
							if (ontology.checkRelation(f, p, "Referto")) {related=true;}
							
							if ((related || p.equals("empty")) && Double.parseDouble(item.split(";")[1]) > max) {
								partSymptomFault = item.split(";")[0];
								max = Double.parseDouble(item.split(";")[1]);
					}}}
					
					if (subClusters.get(partSymptom) == null) {
						faultsOfPartSymptom = new ArrayList<String>();
						faultsOfPartSymptom.add(partSymptomFault);
						subClusters.put(partSymptom, faultsOfPartSymptom);
						
						assigned = new ArrayList<String>();
						assigned.add(record);
						clusters.put(partSymptomFault, assigned);
					}
					else {
						if (subClusters.get(partSymptom).contains(partSymptomFault)) {
							assigned = clusters.get(partSymptomFault);
							assigned.add(record);
							clusters.put(partSymptomFault, assigned);
						}
						else {
							// Create it
							faultsOfPartSymptom = subClusters.get(partSymptom);
							faultsOfPartSymptom.add(partSymptomFault);
							subClusters.put(partSymptom, faultsOfPartSymptom);
							
							assigned = new ArrayList<String>();
							assigned.add(record);
							clusters.put(partSymptomFault, assigned);
						}
					}
				}
			}
		}
	}
	
	public static void run() throws OWLOntologyCreationException, IOException {
		makeSetOfPartSymptomFault();
		makeClusters();
		cluster();
	}
	
	public static Hashtable getClusters() {
		return clusters;
	}
	
	public static Hashtable getSubClusters() {
		return subClusters;
	}
	
	
	public static void main (String[] args) throws IOException, OWLOntologyCreationException {}
}
