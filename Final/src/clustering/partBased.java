package clustering;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import ontology.*;

public class partBased {
	
	static Hashtable<String, ArrayList<String>> clusters = new Hashtable<String, ArrayList<String>>();
	static Hashtable alias = new Hashtable();
	
	// Makes a set of all the parts that are present in the dataset
	private static ArrayList<String> makeSetOfParts() throws FileNotFoundException {
		String part;
		String[] line;
		ArrayList<String> parts = new ArrayList<String>();
		
		BufferedReader reader = new BufferedReader (new FileReader("files/partCriticality"));
		Scanner scanner = new Scanner(reader);
		
		while (scanner.hasNextLine()) {
			line = scanner.nextLine().split(",");
			for (int i=1 ; i<line.length ; i++) {
				part = line[i].split(";")[0];
				if (! parts.contains(part)) { parts.add(part) ; }
		}}
		
		scanner.close();
		return parts;
	}
	
	// Makes a cluster for each part identified in the dataset
	private static void makeClusters() throws FileNotFoundException {
		ArrayList<String> parts = makeSetOfParts();
		ArrayList<String> empty = new ArrayList<String>();
		
		for (String part : parts) {
			ArrayList<String> arr = (ArrayList<String>) empty.clone();
			clusters.put(part, arr);
		}
		
		ArrayList<String> arr = (ArrayList<String>) empty.clone();
		clusters.put("empty", arr);
	}

	// Gets two lists of symptoms and returns how similar they are  (used as metric for average pairwise distance between clusters)
	// The similarity metric is the Jaccard Coefficent  (ie. the cardinality of two sets' intersection over the cardinality of their union)
	private static double similarity(ArrayList<String> s1, ArrayList<String> s2) {
		double similarity = 0;
		
		// Make intersection
		ArrayList<String> intersection = (ArrayList<String>) s1.clone();
		intersection.retainAll(s2);
		
		// Make union
		ArrayList<String> union = (ArrayList<String>) s1.clone();
		for (String s : s2) { if (! union.contains(s)) { union.add(s) ; } }

		// Divide their cardinalities
		if (union.size() > 0) {
			similarity = intersection.size() / (union.size() * 1.0);
			return similarity;
		}
		else { return 0 ; }
	}
	
	// Get the clusters created and merge them according to their distance and the ontology
	private static void mergeClusters() throws IOException, OWLOntologyCreationException {
		boolean mergeMore = true; 
		
		double sumOfDistancesMeasured = 0;
		double sumOfDistances = 0;
		double mergeValue = 0;

		String part1;
		String part2;
		String[] combo;
		
		ArrayList<String> combos = new ArrayList<String>();
		ArrayList<String> voidCombos = new ArrayList<String>();
		ArrayList<String> symptoms1;
		ArrayList<String> symptoms2;
		ArrayList<String> records1;
		ArrayList<String> records2;
		ArrayList<String> coOccurringSymptoms1;
		ArrayList<String> coOccurringSymptoms2;
		ArrayList<String> merged;
		
		Hashtable partSymptoms = extraction.extractor.extractPartSymptom();
		Hashtable toMerge = new Hashtable();
		
		
		while (mergeMore) {
			mergeMore = false;
			
			outerloop:
			for (String cluster1 : clusters.keySet()) {
				for (String cluster2 : clusters.keySet()) {
					if (! cluster1.equals(cluster2)) {
						// If they have not already been marked as a cluster
						if (!voidCombos.contains(cluster1 + "$" + cluster2) && !voidCombos.contains(cluster2 + "$" + cluster1)) {
							// If the reverse hasn't already been looked into
							if (! combos.contains(cluster2 + "$" + cluster1)) {
								
								part1 = cluster1;
								part2 = cluster2;
								records1 = clusters.get(part1);
								records2 = clusters.get(part2);
								
								// For each record combination in those clusters
								for (String record1 : records1) {
									for (String record2 : records2) {
										coOccurringSymptoms1 = new ArrayList<String>();
										coOccurringSymptoms2 = new ArrayList<String>();
										
										symptoms1 = (ArrayList<String>) partSymptoms.get(Integer.parseInt(record1));
										symptoms2 = (ArrayList<String>) partSymptoms.get(Integer.parseInt(record2));
										
										// Get the cooccuring symptoms of the two records for these parts ...
										// ... in the record of the first cluster ...
										for (String symptom1 : symptoms1) { 
											if (symptom1.split("#")[0].equals(part1)) { 
												if (! coOccurringSymptoms1.contains(symptom1.split("#")[1])) {
													coOccurringSymptoms1.add(symptom1.split("#")[1]);
										}}}
										
										// ... and in the record of the second cluster
										for (String symptom2 : symptoms2) { 
											if (symptom2.split("#")[0].equals(part2)) { 
												if (! coOccurringSymptoms2.contains(symptom2.split("#")[1])) {
													coOccurringSymptoms2.add(symptom2.split("#")[1]); 
										}}}
					
										// Add similarity to the total
										sumOfDistances += similarity(coOccurringSymptoms1, coOccurringSymptoms2);
										sumOfDistancesMeasured += 1.0;
									}
								}
								
								// See if the two clusters should be merged or not
								mergeValue = sumOfDistances / sumOfDistancesMeasured ;
								if (mergeValue > 0.75) {			
									mergeMore = true;
									
									// Create a merged cluster
									merged = (ArrayList<String>) records1.clone();
									merged.addAll(records2);
									Set mergedItems = new LinkedHashSet(merged);		// Set created to remove duplicates
									merged.clear();
									merged.addAll(mergedItems);
									
									// Remove previous clusters and add new cluster
									clusters.remove(part1);
									clusters.remove(part2);
									clusters.put( part1 , merged);
									alias.put(part2, part1);
									break outerloop;
								}
								
								// If the clusters are part of the same system, merge them
								boolean same = ontology.checkSuperPart(part1, part2);
								if (same) {
									// Create a merged cluster
									String superPart = ontology.getSuperPart(part1, part2);
									
									ArrayList<String> current = new ArrayList<String>();
									if (toMerge.keySet().contains(superPart)) {
										if (((ArrayList<String>) toMerge.get(superPart)).size() > 0) {
											current = (ArrayList<String>) toMerge.get(superPart);
										}
									}
									current.add(part1);
									current.add(part2);
									toMerge.put(superPart, current);
								}
							}
						}
					}
				}
			}
		}
		
		for (Object o : toMerge.keySet()) {
			String superPart = (String) o;
			ArrayList<String> recs = new ArrayList<String>();
			
			for (String s : (ArrayList<String>) toMerge.get(superPart)) {
				if (clusters.get(s) != null) {
					for (String rec : clusters.get(s)) {
						recs.add(rec);
					}
					clusters.remove(s);
					alias.put(s, superPart);
				}
			}
			clusters.put(superPart, recs);
		}
	}
	
	// Creates the initial (before merging) clusters and populates the relevant dictionary of them
	private static void cluster() throws FileNotFoundException {
		double criticality;
		double max;
				
		String recordNumber;
		String part;
		String label = "";
		String[] record;
		
		ArrayList<String> parts = makeSetOfParts();
		ArrayList<String> foundPart;
		ArrayList<Double> foundCriticality;
		ArrayList<String> existing;
		
		BufferedReader reader = new BufferedReader (new FileReader ("files/partCriticality"));
		Scanner scanner = new Scanner (reader);
		
		// For each record
		while (scanner.hasNextLine()) {
			max = 0;
			label = "";
			foundPart = new ArrayList<String>();
			foundCriticality = new ArrayList<Double>();
			
			record = scanner.nextLine().split(",");
			recordNumber = record[0];
			
			// Find the parts and their criticality
			for (int i=1 ; i<record.length ; i++) {
				part = record[i].split(";")[0];
				criticality = Double.parseDouble(record[i].split(";")[1]);

				foundPart.add(part);
				foundCriticality.add(criticality);
			}
			
			// Find the part with the maximum criticality 
			if (foundPart.size() > 0) {
				for (int j=0 ; j<foundCriticality.size() ; j++) {
					if (foundCriticality.get(j) > max) {
						max = foundCriticality.get(j);
						label = foundPart.get(j);
					}
				}
				
				// Assign to cluster
				if (clusters.get(label) != null) { existing = clusters.get(label); }
				else { existing = new ArrayList<String>(); }
				existing.add(recordNumber);
				clusters.put (label, existing);
			}
			else {
				if (clusters.get("empty") != null) { existing = clusters.get("empty"); }
				else { existing = new ArrayList<String>(); }
				existing.add(recordNumber);
				clusters.put ("empty", existing);
			}
		}
		scanner.close();
	}

	public static void run() throws IOException, OWLOntologyCreationException {
		makeClusters();
		cluster();
		mergeClusters();
	}
	
	public static Hashtable<String, ArrayList<String>> getClusters() {
		return clusters;
	}
	
	public static Hashtable getAlias() {
		return alias;
	}
	
	
	
 	public static void main (String[] args) throws IOException, OWLOntologyCreationException {}
}















