package clustering;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

import ontology.ontology;

public class faultBased {
	static Hashtable<String, ArrayList<String>> clusters = new Hashtable<String, ArrayList<String>>();
	
	private static ArrayList<String> makeSetOfFaults() throws IOException {
		String fault;
		String[] line;
		ArrayList<String> faults = new ArrayList<String>();
		
		Hashtable table = extraction.extractor.getFaultCriticality();
		
		for (Object o : table.keySet()) {
			line = ((String) table.get(o)).split(",");
			for (String item : line) {
				if (item.length() > 0) {
					fault = item.split(";")[0];
					if (! faults.contains(fault)) { faults.add(fault); }
				}
			}
		}
		
		return faults;
	}
	
	private static void makeClusters() throws IOException {
		ArrayList<String> faults = makeSetOfFaults();
		ArrayList<String> empty = new ArrayList<String>();
		
		for (String fault : faults) {
			ArrayList<String> arr = (ArrayList<String>) empty.clone();
			clusters.put(fault, arr);
		}
	}
	
	private static void cluster() throws IOException {
		double criticality;
		double max;
				
		String recordNumber;
		String fault;
		String label = "";
		String[] record;
		
		ArrayList<String> parts = makeSetOfFaults();
		ArrayList<String> foundFault;
		ArrayList<Double> foundCriticality;
		ArrayList<String> existing;
		
		Hashtable table = extraction.extractor.getFaultCriticality();
		
		// For each record
		for (Object o : table.keySet()) {
			max = 0;
			label = "";
			foundFault = new ArrayList<String>();
			foundCriticality = new ArrayList<Double>();
			
			record = ((String) table.get(o)).split(",");
			recordNumber = (String) o;
			
			// Find the faults and their criticality
			for (String item : record) {
				if (item.length() > 0) {
					fault = item.split(";")[0];
					criticality = Double.parseDouble(item.split(";")[1]);

					foundFault.add(fault);
					foundCriticality.add(criticality);
				}
			}
			
			// Find the fault with the maximum criticality 
			if (foundFault.size() > 0) {
				for (int j=0 ; j<foundCriticality.size() ; j++) {
					if (foundCriticality.get(j) > max) {
						max = foundCriticality.get(j);
						label = foundFault.get(j);
					}
				}
				
				// Assign to cluster
				if (clusters.get(label) != null) { existing = clusters.get(label); }
				else { existing = new ArrayList<String>(); }
				existing.add(recordNumber);
				clusters.put (label, existing);
			}
		}
	}
	
	
	public static void run() throws IOException {
		makeSetOfFaults();
		makeClusters();
		cluster();
	}
	
	public static Hashtable getClusters() {
		return clusters;
	}
	
	public static void main(String[] args) throws IOException {}
}
