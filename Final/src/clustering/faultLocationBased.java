package clustering;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import extraction.extractor;
import ontology.ontology;

public class faultLocationBased {
	static Hashtable<String, ArrayList<String>> clusters = new Hashtable<String, ArrayList<String>>();
	static Hashtable<String, ArrayList<String>> subClusters = new Hashtable<String, ArrayList<String>>();
	
	public static ArrayList<String> makeSetOfFaultLocation() throws IOException {
		String faultLocation;
		String[] line;
		ArrayList<String> faultLocations = new ArrayList<String>();
		
		Hashtable table = extraction.extractor.getFaultLocationCriticality();
		
		for (Object o : table.keySet()) {
			line = ((String) table.get(o)).split(",");
			for (String item : line) {
				if (item.length() > 0) {
					faultLocation = item.split(";")[0];
					if (! faultLocations.contains(faultLocation)) { faultLocations.add(faultLocation); }
		}}}
		
		return faultLocations;
	}
	
	public static void makeClusters() throws IOException {
		ArrayList<String> faultLocations = makeSetOfFaultLocation();
		ArrayList<String> empty = new ArrayList<String>();
		
		for (String faultAction : faultLocations) {
			ArrayList<String> arr = (ArrayList<String>) empty.clone();
			clusters.put(faultAction, arr);
		}
	}
	
	private static void cluster() throws IOException, OWLOntologyCreationException {
		double max;
		
		String fault;
		String faultLocation;
		String[] line;
		
		ArrayList<String> records;
		ArrayList<String> locationsForFault;
		ArrayList<String> assigned;
		
		Hashtable faultClusters = faultBased.getClusters();
		
		for (Object o : faultClusters.keySet()) {
			fault = (String) o;
			records = (ArrayList<String>) faultClusters.get(o);
			
			for (String record : records) {
				faultLocation = "";
				max = 0;
				
				if (extractor.getFaultLocationCriticality().get(record).length() > 0) {
					line = extractor.getFaultLocationCriticality().get(record).split(",");
					
					for (String item : line) {
						if (item.split("#")[0].equals(fault)) {
							boolean related = false;
							String f = item.split(";")[0].split("#")[0];
							String l = item.split(";")[0].split("#")[1];
							
							if (ontology.checkRelation(f, l, "Referto")) { related = true; }
							
							if (related && Double.parseDouble(item.split(";")[1]) > max) {
								faultLocation = item.split(";")[0];
								max = Double.parseDouble(item.split(";")[1]);
					}}}
					
					if (subClusters.get(fault) == null) {
						locationsForFault = new ArrayList<String>();
						locationsForFault.add(faultLocation);
						subClusters.put(fault, locationsForFault);
						
						assigned = new ArrayList<String>();
						assigned.add(record);
						clusters.put(faultLocation, assigned);
					}
					else {
						if (subClusters.get(fault).contains(faultLocation)) {
							assigned = clusters.get(faultLocation);
							assigned.add(record);
							clusters.put(faultLocation, assigned);
						}
						else {
							locationsForFault = subClusters.get(fault);
							locationsForFault.add(faultLocation);
							subClusters.put(fault, locationsForFault);
							
							assigned = new ArrayList<String>();
							assigned.add(record);
							clusters.put(faultLocation, assigned);
		
						}
					}
				}
			}
		}
		
	}
	
	public static void run() throws OWLOntologyCreationException, IOException {
		makeSetOfFaultLocation();
		makeClusters();
		cluster();
	}
	
	public static Hashtable getClusters() {
		return clusters;
	}
	
	public static Hashtable getSubClusters() {
		return subClusters;
	}
		
	public static void main (String[] args) throws IOException, OWLOntologyCreationException {}
}
