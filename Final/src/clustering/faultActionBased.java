package clustering;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import extraction.extractor;
import ontology.ontology;

public class faultActionBased {
	static Hashtable<String, ArrayList<String>> clusters = new Hashtable<String, ArrayList<String>>();
	static Hashtable<String, ArrayList<String>> subClusters = new Hashtable<String, ArrayList<String>>();
	
	// Make fault-action combinations found
	public static ArrayList<String> makeSetOfFaultAction() throws IOException {
		String faultAction;
		String[] line;
		ArrayList<String> faultActions = new ArrayList<String>();
		
		Hashtable table = extraction.extractor.getFaultActionCriticality();
		
		for (Object o : table.keySet()) {
			line = ((String) table.get(o)).split(",");
			for (String item : line) {
				if (item.length() > 0) {
					faultAction = item.split(";")[0];
					if (! faultActions.contains(faultAction)) { faultActions.add(faultAction); }
		}}}
		
		return faultActions;
	}
	
	// Make empty clusters
	public static void makeClusters() throws IOException {
		ArrayList<String> faultActions = makeSetOfFaultAction();
		ArrayList<String> empty = new ArrayList<String>();
		
		for (String faultAction : faultActions) {
			ArrayList<String> arr = (ArrayList<String>) empty.clone();
			clusters.put(faultAction, arr);
		}
	}
	
	// Perform clustering
	private static void cluster() throws IOException, OWLOntologyCreationException {
		double max;
		
		String fault;
		String faultAction;
		String[] line;
		
		ArrayList<String> records;
		ArrayList<String> actionsForFault;
		ArrayList<String> assigned;
		
		Hashtable faultClusters = faultBased.getClusters();
		
		// For each fault cluster
		for (Object o : faultClusters.keySet()) {
			fault = (String) o;
			records = (ArrayList<String>) faultClusters.get(o);
			
			// For each record in the cluster
			for (String record : records) {
				faultAction = "";
				max = 0;
				
				// Make sure it's not an empty record
				if (extractor.getFaultActionCriticality().get(record).length() > 0) {
					line = extractor.getFaultActionCriticality().get(record).split(",");
					
					// Find the actions that cooccur with the fault and choose the one with the maximum criticality
					for (String item : line) {
						if (item.split("#")[0].equals(fault)) {
							boolean related = false;
							String f = item.split(";")[0].split("#")[0];
							String a = item.split(";")[0].split("#")[1];

							// Are they related?
							if (ontology.checkRelation(a, f, "Act2FM")) { related = true; }
							
							if (related && Double.parseDouble(item.split(";")[1]) > max) {
								faultAction = item.split(";")[0];
								max = Double.parseDouble(item.split(";")[1]);
					}}}
					
					// If fault doesn't exist in subclusters already
					if (subClusters.get(fault) == null) {
						// Create fault/fault-action entry in subclusters
						actionsForFault = new ArrayList<String>();
						actionsForFault.add(faultAction);
						subClusters.put(fault, actionsForFault);
						
						// Assign record to fault-action
						assigned = new ArrayList<String>();
						assigned.add(record);
						clusters.put(faultAction, assigned);
					}
					// If fault already exists in subclusters
					else {
						if (subClusters.get(fault).contains(faultAction)) {
							assigned = clusters.get(faultAction);
							assigned.add(record);
							clusters.put(faultAction, assigned);
						}
						else {
							actionsForFault = subClusters.get(fault);
							actionsForFault.add(faultAction);
							subClusters.put(fault, actionsForFault);
							
							assigned = new ArrayList<String>();
							assigned.add(record);
							clusters.put(faultAction, assigned);
						}
					}
				}
			}
		}
	}
		
	public static void run() throws OWLOntologyCreationException, IOException {
		makeSetOfFaultAction();
		makeClusters();
		cluster();
	}
	
	public static Hashtable getClusters() {
		return clusters;
	}
	
	public static Hashtable getSubClusters() {
		return subClusters;
	}
	
	public static void main (String[] args) throws IOException, OWLOntologyCreationException {}
}
