package ontology;

import java.io.File;
import java.util.Hashtable;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.vocab.OWLRDFVocabulary;

public class ontology {
	
	// The dictionaries where the data is stored
	static Hashtable symptoms = new Hashtable();
	static Hashtable actions  = new Hashtable();
	static Hashtable faults   = new Hashtable();
	static Hashtable parts    = new Hashtable();
	
	// Gets all the data from the ontology 
	public static void importData() throws OWLOntologyCreationException {
		String subClass;
		String symptom;
		String action;
		String fault;
		String part;
		String defined;
		
		// The basics
		File file = new File ("files/FirstVehicle.owl");
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		OWLOntology ontology = manager.loadOntologyFromOntologyDocument(file);
	    OWLDataFactory factory = manager.getOWLDataFactory();
	    OWLAnnotationProperty keywords = factory.getOWLAnnotationProperty(OWLRDFVocabulary.RDFS_IS_DEFINED_BY.getIRI());
		String url = "http://www.owl-ontologies.com/FirstVehicle.owl#";
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		// Get symptoms and their keywords
		IRI symptomsIRI = IRI.create(url + "Symptom");
		OWLClass sym = factory.getOWLClass(symptomsIRI);
		Set<OWLClassExpression> sub = sym.getSubClasses(ontology);
		
		// For all subclasses of symptom, get their subclasses
		for (OWLClassExpression cls : sub) {
			subClass = cls.toString().substring(1,cls.toString().length()-1);
			IRI subIRI = IRI.create(subClass);
			OWLClass subSymptoms = factory.getOWLClass(subIRI);
			
			// Get the names and isDefinedBy annotations from them and add them to a dictionary
			Set<OWLClassExpression> sub2 = subSymptoms.getSubClasses(ontology);
			for (OWLClassExpression cls2 : sub2) {
				symptom = "";
				defined = "";
				
				symptom = cls2.toString().split("#")[1].substring(0,cls2.toString().split("#")[1].length()-1);
				
				Set<OWLAnnotation> ann = cls2.asOWLClass().getAnnotations(ontology, keywords);
				for (OWLAnnotation a : ann) { defined = a.getValue().toString(); }
				
				// Make sure the values are not empty before keeping them
				if (!symptom.equals("") && !defined.equals("")) { symptoms.put(symptom, defined); }
			}
		}
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		// Get actions and their keywords
		IRI actionsIRI = IRI.create(url + "Action");
		OWLClass act = factory.getOWLClass(actionsIRI);
		Set<OWLClassExpression> subActions = act.getSubClasses(ontology);
		
		for (OWLClassExpression o : subActions) {
			action  = "";
			defined = "";
			
			action = o.toString().split("#")[1].substring(0,o.toString().split("#")[1].length()-1);
			
			Set<OWLAnnotation> ann2 = o.asOWLClass().getAnnotations(ontology, keywords);
			for (OWLAnnotation a : ann2) { defined = a.getValue().toString(); }
			
			if (!action.equals("") && !defined.equals("")) { actions.put(action, defined); }
		}
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		// Get actions and their keywords
		IRI faultIRI = IRI.create(url + "FaultMode");
		OWLClass f = factory.getOWLClass(faultIRI);
		Set<OWLClassExpression> subFaults = f.getSubClasses(ontology);
		
		for (OWLClassExpression o : subFaults) {
			fault   = "";
			defined = "";
			
			fault = o.toString().split("#")[1].substring(0,o.toString().split("#")[1].length()-1);
			
			if (fault.equals("STU_VFault")) {								// STU is the only fault mode that has subclasses
				IRI stuIRI = IRI.create(url + fault);
				OWLClass stu = factory.getOWLClass(stuIRI);
				
				Set<OWLAnnotation> ann10 = stu.getAnnotations(ontology, keywords);
				for (OWLAnnotation oan : ann10) { defined = oan.getValue().toString(); }
				if (!fault.equals("") && !defined.equals("")) { faults.put(fault, defined); }
				
				Set<OWLClassExpression> subSTU = stu.getSubClasses(ontology);
				for (OWLClassExpression c : subSTU) {
					fault = "";
					defined = "";
					
					fault = c.toString().split("#")[1].substring(0,c.toString().split("#")[1].length()-1);
					
					Set<OWLAnnotation> ann4 = c.asOWLClass().getAnnotations(ontology, keywords);
					for (OWLAnnotation an : ann4) { defined = an.getValue().toString(); }
					
					if (!fault.equals("") && !defined.equals("")) { faults.put(fault, defined); }
				}
			}
			else {
				Set<OWLAnnotation> ann3 = o.asOWLClass().getAnnotations(ontology, keywords);
				for (OWLAnnotation a : ann3) { defined = a.getValue().toString(); }
				
				if (!fault.equals("") && !defined.equals("")) { faults.put(fault, defined); }
			}
		}
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		// Get parts and their keywords
		IRI partIRI = IRI.create(url + "OnBoardSystem");
		OWLClass p = factory.getOWLClass(partIRI);
		Set<OWLClassExpression> subParts = p.getSubClasses(ontology);
		
		for (OWLClassExpression o : subParts) {
			part    = "";
			defined = "";
			
			part = o.toString().split("#")[1].substring(0,o.toString().split("#")[1].length()-1);
			
			// If the entity has subclasses
			if (o.asOWLClass().getSubClasses(ontology).size() > 0) {
				IRI newIRI = IRI.create(url + part);
				OWLClass newClass = factory.getOWLClass(newIRI);
				Set<OWLClassExpression> newSubs = newClass.getSubClasses(ontology);
				
				Set<OWLAnnotation> ann8 = newClass.getAnnotations(ontology, keywords);
				for (OWLAnnotation oan : ann8) { defined = oan.getValue().toString(); }
				if (!part.equals("") && !defined.equals("")) { parts.put(part, defined); }
				
				for (OWLClassExpression c : newSubs) {
					part = c.toString().split("#")[1].substring(0,c.toString().split("#")[1].length()-1);
					
					// If that subclass is STU_V (which has further subclasses)
					if (part.equals("STU_V")) {
						IRI stIRI = IRI.create(url + "STU_V");
						OWLClass st = factory.getOWLClass(stIRI);
						Set<OWLClassExpression> stSubs = st.getSubClasses(ontology);
						
						Set<OWLAnnotation> ann9 = st.getAnnotations(ontology, keywords);
						for (OWLAnnotation oann : ann9) { defined = oann.getValue().toString(); }
						if (!part.equals("") && !defined.equals("")) { parts.put(part, defined); }
						
						for (OWLClassExpression e : stSubs) {
							part = "";
							defined = "";
							
							part = e.toString().split("#")[1].substring(0,e.toString().split("#")[1].length()-1);
							
							Set<OWLAnnotation> ann5 = e.asOWLClass().getAnnotations(ontology, keywords);
							for (OWLAnnotation a : ann5) { defined = a.getValue().toString(); }
							
							if (!part.equals("") && !defined.equals("")) { parts.put(part, defined); }
						}
					}
					// Otherwise
					else {
						defined = "";
						Set<OWLAnnotation> ann6 = c.asOWLClass().getAnnotations(ontology, keywords);
						for (OWLAnnotation oa : ann6) { defined = oa.getValue().toString() ; }
						
						if (!part.equals("") && !defined.equals("")) { parts.put(part, defined); }
					}
				}
			}
			// Entity has no subclasses
			else {
				Set<OWLAnnotation> ann7 = o.asOWLClass().getAnnotations(ontology, keywords);
				for (OWLAnnotation a : ann7) { defined = a.getValue().toString(); }
				
				if (!part.equals("") && !defined.equals("")) { parts.put(part, defined); }
			}
		}
	}
	
	// Getters
	public static Hashtable getSymptoms() { return symptoms; }
	public static Hashtable getActions() { return actions; }
	public static Hashtable getFaults() { return faults; }
	public static Hashtable getParts() { return parts; }
	
	// Checks if two entities in the ontology are related
	public static boolean checkRelation (String class_one, String class_two, String relation) throws OWLOntologyCreationException {
		// Basics
		String url = "http://www.owl-ontologies.com/FirstVehicle.owl#";
		File file = new File ("files/FirstVehicle.owl");
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
	    OWLOntology ontology = manager.loadOntologyFromOntologyDocument(file);
	    OWLDataFactory factory = manager.getOWLDataFactory();
	    
	    // Get the class from the first string
	    IRI iri_one = IRI.create(url + class_one);
	    OWLClass one = factory.getOWLClass(iri_one);
	    
	    // Check if they are related
	    Set<OWLClassExpression> list = one.getSuperClasses(ontology);
	    for (OWLClassExpression e : list) {
	    	if 		(e.toString().equals( "ObjectAllValuesFrom(<" + url + relation + "> <" + url + class_two + ">)" )) { return true; }
	    	else if (e.toString().equals( "ObjectSomeValuesFrom(<" + url + relation + "> <" + url + class_two + ">)" )) { return true; }
	    }
		
	    // Return false if they're not related
		return false;
	}
	
	public static boolean observeSameFM(String class_one, String class_two) throws OWLOntologyCreationException {
		// Basics
		String url = "http://www.owl-ontologies.com/FirstVehicle.owl#";
		File file = new File ("files/FirstVehicle.owl");
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
	    OWLOntology ontology = manager.loadOntologyFromOntologyDocument(file);
	    OWLDataFactory factory = manager.getOWLDataFactory();
	    
	    // Get the class from the first string
	    IRI iri_one = IRI.create(url + class_one);
	    OWLClass one = factory.getOWLClass(iri_one);
	    IRI iri_two = IRI.create(url + class_two);
	    OWLClass two = factory.getOWLClass(iri_two);
	    
	    Set<OWLClassExpression> list_one = one.getSuperClasses(ontology);
	    Set<OWLClassExpression> list_two = two.getSuperClasses(ontology);
	    
	    for (OWLClassExpression e : list_one) {
	    	String value = e.toString().split(" ")[0];
	    	String relation = value.substring(value.length()-7, value.length()-1);
	    	if (relation.equals("Sym2FM")) {
	    		if (list_two.contains(e)) {
	    			return true;
	    		}
	    	}
	    }
	    
	    return false;
	}
	
	public static boolean checkSuperPart(String part1, String part2) throws OWLOntologyCreationException {
		// Basics
		String url = "http://www.owl-ontologies.com/FirstVehicle.owl#";
		File file = new File ("files/FirstVehicle.owl");
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
	    OWLOntology ontology = manager.loadOntologyFromOntologyDocument(file);
	    OWLDataFactory factory = manager.getOWLDataFactory();

	    // Make classes
	    IRI iri_one = IRI.create(url + part1);
	    IRI iri_two = IRI.create(url + part2);
	    OWLClass one = factory.getOWLClass(iri_one);
	    OWLClass two = factory.getOWLClass(iri_two);
	    
	    // Get their superclasses
	    Set<OWLClassExpression> super_one = one.getSuperClasses(ontology);
	    Set<OWLClassExpression> super_two = two.getSuperClasses(ontology);
	    
	    // If they have a common superclass
	    for (OWLClassExpression e : super_one) {
	    	if (super_two.contains(e)) {
	    		// And that superclass is not OnBoardSystem (which is a superclass for all parts)
	    		if (! e.toString().equals("<" + url + "OnBoardSystem>")) { return true; }
	    	}
	    }
	    
	    return false;
	}
	
	public static String getSuperPart(String part1, String part2) throws OWLOntologyCreationException {
		String part = "";
		
		// Basics
		String url = "http://www.owl-ontologies.com/FirstVehicle.owl#";
		File file = new File ("files/FirstVehicle.owl");
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
	    OWLOntology ontology = manager.loadOntologyFromOntologyDocument(file);
	    OWLDataFactory factory = manager.getOWLDataFactory();

	    // Make classes
	    IRI iri_one = IRI.create(url + part1);
	    IRI iri_two = IRI.create(url + part2);
	    OWLClass one = factory.getOWLClass(iri_one);
	    OWLClass two = factory.getOWLClass(iri_two);
	    
	    // Get their superclasses
	    Set<OWLClassExpression> super_one = one.getSuperClasses(ontology);
	    Set<OWLClassExpression> super_two = two.getSuperClasses(ontology);
	    
	    // If they have a common superclass
	    for (OWLClassExpression e : super_one) {
	    	if (super_two.contains(e)) {
	    		// And that superclass is not OnBoardSystem (which is a superclass for all parts)
	    		if (! e.toString().equals("<" + url + "OnBoardSystem>")) {
	    			String s = e.toString().split("#")[1];
	    			part = s.substring(0, s.length()-1); 
	    		}
	    	}
	    }
	    
	    return part;
	}
	
	public static boolean checkSuperFault(String fault1, String fault2) throws OWLOntologyCreationException {
		// Basics
		String url = "http://www.owl-ontologies.com/FirstVehicle.owl#";
		File file = new File ("files/FirstVehicle.owl");
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
	    OWLOntology ontology = manager.loadOntologyFromOntologyDocument(file);
	    OWLDataFactory factory = manager.getOWLDataFactory();

	    // Make classes
	    IRI iri_one = IRI.create(url + fault1);
	    IRI iri_two = IRI.create(url + fault2);
	    OWLClass one = factory.getOWLClass(iri_one);
	    OWLClass two = factory.getOWLClass(iri_two);
	    
	    // Get their superclasses
	    Set<OWLClassExpression> super_one = one.getSuperClasses(ontology);
	    Set<OWLClassExpression> super_two = two.getSuperClasses(ontology);
	    
	    // If they have a common superclass
	    for (OWLClassExpression e : super_one) {
	    	if (super_two.contains(e)) {
	    		// And that superclass is not FaultMode (which is a superclass for all parts)
	    		if (! e.toString().equals("<" + url + "FaultMode>")) { return true; }
	    	}
	    }
	    
	    return false;
	}
	
	public static String getSuperFault(String fault1, String fault2) throws OWLOntologyCreationException {
		String fault = "";
		
		// Basics
		String url = "http://www.owl-ontologies.com/FirstVehicle.owl#";
		File file = new File ("files/FirstVehicle.owl");
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
	    OWLOntology ontology = manager.loadOntologyFromOntologyDocument(file);
	    OWLDataFactory factory = manager.getOWLDataFactory();

	    // Make classes
	    IRI iri_one = IRI.create(url + fault1);
	    IRI iri_two = IRI.create(url + fault2);
	    OWLClass one = factory.getOWLClass(iri_one);
	    OWLClass two = factory.getOWLClass(iri_two);
	    
	    // Get their superclasses
	    Set<OWLClassExpression> super_one = one.getSuperClasses(ontology);
	    Set<OWLClassExpression> super_two = two.getSuperClasses(ontology);
	    
	    // If they have a common superclass
	    for (OWLClassExpression e : super_one) {
	    	if (super_two.contains(e)) {
	    		// And that superclass is not FaultMode (which is a superclass for all parts)
	    		if (! e.toString().equals("<" + url + "FaultMode>")) {
	    			String s = e.toString().split("#")[1];
	    			fault = s.substring(0, s.length()-1); 
	    		}
	    	}
	    }
	    
	    return fault;
	}
	
	public static void main (String[] args) throws OWLOntologyCreationException {}
}
