package annotation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Scanner;
import java.util.StringTokenizer;

import ontology.ontology;

import org.xml.sax.*;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.w3c.dom.*;

import javax.xml.parsers.*;


public class annotation {
	// Opens the dataset xml
	public static Document getDocument(String docString) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setIgnoringComments(true);
			factory.setIgnoringElementContentWhitespace(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			return builder.parse(new InputSource(docString));
		}
		catch (Exception ex) { System.out.println(ex.getMessage()); }
		
		return null;
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////
	
	// Takes a string as input and tokenizes it
	public static ArrayList<String> tokenize(String text) {
		ArrayList<String> tokens = new ArrayList<String> ();
		StringTokenizer tokenizer = new StringTokenizer(text);
		while (tokenizer.hasMoreTokens()) { tokens.add(tokenizer.nextToken()); }
	    return tokens;
	}
	
	// Creates and returns an array list of all stop words
	private static ArrayList <String> makeStopWordList () {
		try {
			ArrayList <String> stopwords = new ArrayList <String> ();
			
			BufferedReader reader = new BufferedReader(new FileReader("files/stopwords.txt"));
			Scanner scanner = new Scanner(reader).useDelimiter(",");
			while (scanner.hasNext()) {
				stopwords.add(scanner.next());
			}
			scanner.close();
			
			return stopwords;
		} 
		catch (FileNotFoundException e) { e.printStackTrace(); }
		
		return null;
	}
	
	// Removes all stop words from a tokenized string	
	public static void stopWordDelete (ArrayList<String> tokenizedText) {
		ArrayList <String> stopwords = makeStopWordList();
		
		Iterator<String> i = tokenizedText.iterator();
		while (i.hasNext()) {
			String s = i.next();
			s = s.toLowerCase();
			if (stopwords.contains(s)) { i.remove(); }
		}
	}
	
	// Removes certain special characters from the beginning or the end of a token
	public static void cleanUp (ArrayList<String> tokens) {
		boolean start;
		boolean end;
		
		String s;
		ArrayList<String> toAdd = new ArrayList<String>();
		
		Iterator<String> i = tokens.iterator();
		
		while (i.hasNext()) {
			start = false;
			end = false;
			
			s = i.next();
			char first = s.charAt(0);
			char last = s.charAt(s.length() -1);
			
			if (first == '"' || first == '\'' || first == '(' || first == '+' || first == '-') 	{ start = true; }
			if (last == '"' || last == '\'' || last == ')' || last == '+' || last == '-') 		{ end   = true; }
			
			if (start && end) {
				if (s.length() > 1) { toAdd.add(s.substring(1,s.length()-1)); }
				i.remove();
			}
			else if (start) {
				toAdd.add(s.substring(1));
				i.remove();
			}
			else if (end) {
				toAdd.add(s.substring(0,s.length()-1));
				i.remove();
			}
		}
		
		for (String t : toAdd) { tokens.add(t); }
	}
	
	// Separates phrases and sentences in a string
	public static String[] separateSentences (String s) {
		String[] sentences = s.split(",|\\.");
		return sentences;
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////
	
	// Finds parts in a tokenized record and adds them to the annotated dataset
	private static void partFinder(ArrayList<String> tokens, BufferedWriter writer) throws IOException {
		String key;
		String value;
		ArrayList<String> keywords;
		ArrayList<String> found;
		Hashtable parts = ontology.getParts();
		
		for (Object o : parts.keySet()) {
			keywords = new ArrayList<String>();
			found = new ArrayList<String>();
			
			key = (String) o;
			value = (String) parts.get(o);
			
			for (int i=0 ; i<value.split(",").length ; i++) { keywords.add(value.split(",")[i].substring(1,value.split(",")[i].length()-1)); }
			
			for (String keyword : keywords) {
				String[] versions = keyword.split("\\|");
				for (String token : tokens) {
					keyword = keyword.toLowerCase();
					token = token.toLowerCase();
					if (token.matches(keyword)) {
						boolean exists = false;
						for (String v : versions) { if (found.contains(v)) { exists = true; break; } }
						if (!exists) { found.add(token); }
			}}}
			
			if (found.size() == keywords.size()) { writer.write("<part>" + key + "</part>"); }
		}
	}
	
	// Finds symptoms in a tokenized record and adds them to the annotated dataset
	public static void symptomFinder(ArrayList<String> tokens, BufferedWriter writer) throws IOException {
		if (tokens.size() > 0) {
			String line = "";
			String symptom = "";
			String data = "";
			String[] symptomData;
			
			ArrayList<String> keywords = new ArrayList<String>();
			ArrayList<String> found = new ArrayList<String>();

			writer.write("\n\t<phrase>");
			Hashtable symptoms = ontology.getSymptoms();
			
			for (Object o : symptoms.keySet()) {
				symptom = (String) o;
				data = ((String) symptoms.get(o)).substring(1,((String) symptoms.get(o)).length()-1);
				symptomData = data.split(",");
							
				keywords = new ArrayList<String>();
				found = new ArrayList<String>();
				
				// Get the keywords of this regex
				for (String s : symptomData) { keywords.add(s); }
				
				// And match them against the tokens
				for (String keyword : keywords) {
					String[] versions = keyword.split("\\|");
					for (String token : tokens) {
						keyword = keyword.toLowerCase();
						token = token.toLowerCase();
						if (token.matches(keyword)) {
							boolean exists = false;
							for (String v : versions) { 
								v = v.toLowerCase();
								if (found.contains(v)) { exists = true; break; } 
							}
							if (!exists) { found.add(token); }
				}}}
				
				// If there's a match, add it to the file
				if (found.size() == keywords.size()) {
					writer.write("\n\t\t<symptom>" + symptom + "</symptom>");
				}
			}
			
			// Also find parts and adds them
			partFinder(tokens, writer);
			
			writer.write("\n\t</phrase>");
		}
	}
	
	// Finds actions in a tokenized record and adds them to the annotated dataset
	private static void actionFinder(ArrayList<String> tokens, BufferedWriter writer) throws IOException {
		if (tokens.size() > 0) {
			String line = "";
			String action = "";
			String data = "";
			String[] actionData;
			
			ArrayList<String> keywords = new ArrayList<String>();
			ArrayList<String> found = new ArrayList<String>();

			Hashtable actions = ontology.getActions();
			
			for (Object o : actions.keySet()) {
				action = (String) o;
				data = ((String) actions.get(o)).substring(1,((String) actions.get(o)).length()-1);
				actionData = data.split(",");
							
				keywords = new ArrayList<String>();
				found = new ArrayList<String>();
				
				// Get the keywords of this regex
				for (String s : actionData) { keywords.add(s); }
				
				// And match them against the tokens
				for (String keyword : keywords) {
					String[] versions = keyword.split("\\|");
					for (String token : tokens) {
						keyword = keyword.toLowerCase();
						token = token.toLowerCase();
						if (token.matches(keyword)) {
							boolean exists = false;
							for (String v : versions) { if (found.contains(v)) { exists = true; break; } }
							if (!exists) { found.add(token); }
				}}}
				
				// If there's a match, add it to the file
				if (found.size() == keywords.size()) { 
					writer.write("\n\t<action>" + action + "</action>");
				}
			}
		}
	}
	
	// Finds fault locations in a record and adds them to the annotated dataset
	private static void locationFinder(ArrayList<String> tokens, BufferedWriter writer) throws IOException {
		String text = "";
		String key;
		String value;
		String[] versions;
		Hashtable parts = ontology.getParts();
		
		for (String token : tokens) { text += " " + token; }
		text = text.substring(1);
		text = text.toLowerCase();
		
		for (Object o : parts.keySet()) {
			key = (String) o;
			value = (String) parts.get(o);
			value = value.substring(1,value.length()-1);
			versions = value.split("\\|");
			
			for (String version : versions) {
				version = version.toLowerCase();
				if (text.equals(version)) { writer.write("<location>" + key + "</location>"); }
			}
		}
	}
	
	// Finds first degree faults in a record and adds them to the annotated dataset
	private static void faultFinder(ArrayList<String> tokens, BufferedWriter writer) throws IOException {
		if (tokens.size() > 0) {
			String line = "";
			String fault = "";
			String data = "";
			String[] symptomData;
			
			ArrayList<String> keywords = new ArrayList<String>();
			ArrayList<String> found = new ArrayList<String>();

			Hashtable faults = ontology.getFaults();
			
			for (Object o : faults.keySet()) {
				fault = (String) o;
				data = ((String) faults.get(o)).substring(1,((String) faults.get(o)).length()-1);
				symptomData = data.split(",");
							
				keywords = new ArrayList<String>();
				found = new ArrayList<String>();
				
				// Get the keywords of this regex
				for (String s : symptomData) { keywords.add(s); }
				
				// And match them against the tokens
				for (String keyword : keywords) {
					String[] versions = keyword.split("\\|");
					for (String token : tokens) {
						keyword = keyword.toLowerCase();
						token = token.toLowerCase();
						if (token.matches(keyword)) {
							boolean exists = false;
							for (String v : versions) { if (found.contains(v)) { exists = true; break; } }
							if (!exists) { found.add(token); }
				}}}
				
				// If there's a match, add it to the file
				if (found.size() == keywords.size()) { 
					writer.write("<fault>" + fault + "</fault>");
				}
			}
			
			// Also find parts and adds them
			partFinder(tokens, writer);
		}
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////
	
	// Creates the annotated dataset
	public static void annotate () throws IOException {
		String[] phrases;
		ArrayList<String> tokens;
		
		// Open data set
		Document xmlDoc = getDocument("files/dataset.xml");
		NodeList listOfRecords = xmlDoc.getElementsByTagName("record");

		// Open file writer
		BufferedWriter writer = new BufferedWriter(new FileWriter ("files/annotatedDataSet.xml"));
		writer.write("<dataset>\n");
		
		// Go through all records
		for (int i=0 ; i<listOfRecords.getLength() ; i++) {
			
			Node recordNode = listOfRecords.item(i);
			Element recordElement = (Element) recordNode;
			
			if (i==0) 	{ writer.write("<record>")   ; }
			else 		{ writer.write("\n<record>") ; }
						
			// Description tag
			NodeList descriptionList = recordElement.getElementsByTagName("description");
			if (descriptionList.getLength() >0) {
				String description = descriptionList.item(0).getTextContent();
				
				// Separate into phrases
				phrases = separateSentences(description);
				// Tokenize, remove stop words ...
				for (String phrase : phrases) {
					tokens = tokenize(phrase);
					stopWordDelete (tokens);
					cleanUp(tokens);
					
					// ... and find symptoms
					symptomFinder(tokens, writer);
				}
				
			}
						
			// Analysis tag
			NodeList analysisList = recordElement.getElementsByTagName("analysis");
			if (analysisList.getLength() > 0) {
				String analysis = analysisList.item(0).getTextContent();
				
				phrases = separateSentences(analysis);
				for (String phrase : phrases) {
					tokens = tokenize(phrase);
					stopWordDelete (tokens);
					cleanUp(tokens);
					symptomFinder(tokens, writer);
				}
			}
		
			// Location tag
			NodeList locationList = recordElement.getElementsByTagName("location");
			if (locationList.getLength() > 0) {
				String location = locationList.item(0).getTextContent();
				tokens = tokenize(location);
				stopWordDelete(tokens);
				cleanUp(tokens);
				locationFinder(tokens, writer);
			}
						
			// Fault tag
			NodeList faultList = recordElement.getElementsByTagName("firstfault");
			if (faultList.getLength() > 0) {
				String fault = faultList.item(0).getTextContent();
				tokens = tokenize(fault);
				stopWordDelete(tokens);
				cleanUp(tokens);
				faultFinder(tokens, writer);
			}
						
			// Result tag
			NodeList resultList = recordElement.getElementsByTagName("result");
			if (resultList.getLength() > 0) {
				String result = resultList.item(0).getTextContent();
				
				// Separate into phrases
				phrases = separateSentences(result);
				// Tokenize, remove stop words ...
				for (String phrase : phrases) {
					tokens = tokenize(phrase);
					stopWordDelete (tokens);
					cleanUp(tokens);
					
					// ... and find symptoms
					actionFinder(tokens, writer);
				}
			}
			
			writer.write("\n</record>");
			
		}
		
		writer.write("\n</dataset>");
		writer.close();
	}
	
	
	
	public static void main (String[] args) throws IOException, OWLOntologyCreationException {}

}
