package run;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Map;
import java.util.Scanner;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ontology.*;
import annotation.*;
import extraction.*;
import clustering.*;

public class createKnowledge {
	
	static Hashtable<String, Hashtable> ps_faults = new Hashtable();
	static Hashtable<String, Hashtable> f_locations = new Hashtable();
	static Hashtable<String, Hashtable> f_actions = new Hashtable();
	
	public static void buildData() {
		Hashtable table;
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Build fault finder (out of part-symptom tuples)
		// Get the clusters from part-symptom-fault clustering
		Hashtable psf = partSymptomFaultBased.getClusters();
		Hashtable psf_sub = partSymptomFaultBased.getSubClusters();
		ArrayList<String> faults = new ArrayList<String>();
		
		// For each part-symptom combo
		for (Object o : psf_sub.keySet()) {
			table = new Hashtable();
			faults = (ArrayList<String>) psf_sub.get(o);
			if (faults.size() > 0) {
				// For each fault in that part-symptom combo
				for (String f : faults) {
					if (f.length() > 0) {
						String fault = f.split("#")[2];							// Get the fault
						int freq = ((ArrayList<String>) psf.get(f)).size();		// Get how many times it was observed
						table.put(fault, freq);
					}
				}
			}
			ps_faults.put( (String) o, table );									// Keep it all in a hashtable
		}
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Build location finder (out of fault tuples)
		Hashtable fl = faultLocationBased.getClusters();
		Hashtable fl_sub = faultLocationBased.getSubClusters();
		ArrayList<String> locations = new ArrayList<String>();
		
		for (Object o : fl_sub.keySet()) {
			table = new Hashtable();
			locations = (ArrayList<String>) fl_sub.get(o);
			if (locations.size() > 0) {
				for (String l : locations) {
					if (l.length() > 0) {
						String location = l.split("#")[1];
						int freq = ((ArrayList<String>) fl.get(l)).size();
						table.put(location, freq);
					}
				}
			}
			f_locations.put( (String) o , table );
		}
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Build action finder (out of fault tuples)
		Hashtable fa = faultActionBased.getClusters();
		Hashtable fa_sub = faultActionBased.getSubClusters();
		ArrayList<String> actions = new ArrayList<String>();
		
		for (Object o : fa_sub.keySet()) {
			table = new Hashtable();
			actions = (ArrayList<String>) fa_sub.get(o);
			if (actions.size() > 0) {
				for (String a : actions) {
					if (a.length() > 0) {
						String action = a.split("#")[1];
						int freq = ((ArrayList<String>) fa.get(a)).size();
						table.put(action, freq);
					}
				}
			}
			f_actions.put( (String) o , table );
		}
		
		System.out.println();
		System.out.println("BUILT!");
	}
	
	public static void create() throws OWLOntologyCreationException, IOException {
		ontology.importData();
		System.out.println("ontology done");
		annotation.annotate();
		System.out.println("annotation done");
		extractor.extract();
		System.out.println("extraction done");

		partBased.run();
		partSymptomBased.run();
		partSymptomFaultBased.run();
		faultBased.run();
		faultActionBased.run();
		faultLocationBased.run();
		System.out.println("clustering done");
	}
	
	// Takes a hashtable and sorts it by value
	public static ArrayList<Map.Entry<?, Integer>> sortByValue (Hashtable<?, Integer> table) {
		// http://stackoverflow.com/questions/5176771/sort-hashtable-by-values

		ArrayList<Map.Entry<?, Integer>> list = new ArrayList (table.entrySet());
		
		Collections.sort(list , new Comparator <Map.Entry <?, Integer> > () {
			public int compare (Map.Entry <?, Integer> o1, Map.Entry <?, Integer> o2) {
	            return o1.getValue().compareTo(o2.getValue());
	        }
		});

		return list;
	}
	
	// Takes a hashtable and get the total number of records assigned
	public static double getTotal(Hashtable table) {
		double i = 0;
		
		for (Object o : table.keySet()) {
			i += (Integer) table.get(o) ;
		}
		
		return i;
	}
	
	public static ArrayList<String> topResults (Hashtable table) {
		ArrayList list = sortByValue(table);
		double total = getTotal(table);
		String result = "";
		ArrayList<String> r = new ArrayList<String>();
		
		if (list.size() >= 3) {
			for (int i=1 ; i<=3 ; i++) {
				Object item = list.get(list.size()-i);
				String entity = item.toString().split("=")[0];
				double probability = (Double.parseDouble(item.toString().split("=")[1]) / (total * 1.0)) * 100;
				if (probability == 100) { probability = 99.0; }
				DecimalFormat df = new DecimalFormat("#.##");
				String prob = df.format(probability);
				result += ( entity + " : " + prob + "%\n");
				r.add(entity + " :  " + prob + " %");
			}
		}
		else {
			for (int i=1 ; i<=list.size() ; i++) {
				Object item = list.get(list.size()-i);
				String entity = item.toString().split("=")[0];
				double probability = (Double.parseDouble(item.toString().split("=")[1]) / (total * 1.0)) * 100;
				if (probability == 100) { probability = 99.0; }
				DecimalFormat df = new DecimalFormat("#.##");
				String prob = df.format(probability);
				result += ( entity + " : " + prob + "%\n");
				r.add(entity + " :  " + prob + " %");
			}
		}
		
		return r;
	}
	
	public static ArrayList<String> findFault(String partSymptom) {
		if (ps_faults.get(partSymptom) == null) {
			Hashtable t = partSymptomBased.getAlias();
			if (t.keySet().contains(partSymptom)) {
				partSymptom = (String) t.get(partSymptom);
			}
			else if (! partBased.getClusters().keySet().contains(partSymptom.split("#")[0])) {
				if (partBased.getAlias().keySet().contains(partSymptom.split("#")[0])) {
					partSymptom = ((String) partBased.getAlias().get(partSymptom.split("#")[0])) + "#" + partSymptom.split("#")[1];
				}
			}
		}
		
		if (ps_faults.get(partSymptom) != null) {
			Hashtable table = ps_faults.get(partSymptom);
			ArrayList<String> result = topResults(table);
			return result;
		}
		else { return null; }
	}
	
	public static ArrayList<String> findLocation(String fault) {
		if (f_locations.get(fault) != null) {
			Hashtable table = f_locations.get(fault);
			ArrayList<String> result = topResults(table);
			return result;
		}
		else { return null; }
	}

	public static ArrayList<String> findAction(String fault) {
		if (f_actions.get(fault) != null) {
			Hashtable table = f_actions.get(fault);
			ArrayList<String> result = topResults(table);
			return result;
		}
		else { return null; }
	}
	
	public static Hashtable mineText(String text) throws IOException, OWLOntologyCreationException {
		if (text.length() > 0) {
			// Make xml with part and symptom tags
			BufferedWriter writer = new BufferedWriter ( new FileWriter ("files/current.xml") );
			writer.write("<entry>");
			
			String[] phrases = annotation.separateSentences(text);
			for (String phrase : phrases) {
				ArrayList<String> tokens = annotation.tokenize(phrase);
				annotation.stopWordDelete (tokens);
				annotation.cleanUp(tokens);
				
				annotation.symptomFinder(tokens, writer);
			}
			
			writer.write("</entry>");
			writer.close();
			
			// Extract tuples			
			Hashtable table = new Hashtable<>();
			
			ArrayList<String> parts = new ArrayList<String>();
			ArrayList<String> symptoms = new ArrayList<String>();
			ArrayList<String> partsFound = new ArrayList<String>();
			ArrayList<String> symptomsFound = new ArrayList<String>();
			ArrayList<String> combos = new ArrayList<String>();
			
			Document xmlDoc = annotation.getDocument("files/current.xml");
			NodeList phraseList = xmlDoc.getElementsByTagName("phrase");
			
			if (phraseList.getLength() > 0) {
				for (int j=0 ; j<phraseList.getLength() ; j ++) {
					parts = new ArrayList<String>();
					symptoms = new ArrayList<String>();
					
					NodeList list = phraseList.item(j).getChildNodes();
					
					for (int k=0 ; k < list.getLength() ; k++) {
						if 	(list.item(k).getNodeName() == "part") 		{ parts.add(list.item(k).getTextContent())		; }
						if (list.item(k).getNodeName() == "symptom") 	{ symptoms.add(list.item(k).getTextContent())	; }
					}
					
					if (parts.size()>0 && symptoms.size()>0) {
						for (String symptom : symptoms) {
							boolean p = false;
							for (String part : parts) {
								if (ontology.checkRelation(symptom, part, "Referto")) {
									combos.add(part + "#" + symptom);
									p = true;
								}
							}
							if (!p) {combos.add("empty#" + symptom); }
						}
					}
					else if (parts.size() == 0 && symptoms.size() > 0) {
						for (String symptom : symptoms) {
							combos.add("empty#" + symptom);
						}
					}
					for (String part : parts) 		{ partsFound.add(part); }
					for (String symptom : symptoms) { symptomsFound.add(symptom); }
					
				}
			}

			// Find tuple frequencies
			Hashtable frequencies = new Hashtable();
			for (String combo : combos) {
				Double d = (Collections.frequency(combos, combo)) * 100.0 / (combos.size() * 1.0);
				DecimalFormat df = new DecimalFormat("#.##");
				frequencies.put(combo, df.format(d));
			}
			
			return frequencies;
		}
		else { return null; }
	}
	
	public static void addRecord(String description, String analysis, String location, String fault, String action) throws IOException {
		String line = "";
		File old_file = new File ("files/dataset.xml");
		File new_file = new File ("files/temp");
		
		BufferedReader reader = new BufferedReader ( new FileReader (old_file) );
		Scanner scanner = new Scanner (reader);
		
		BufferedWriter writer = new BufferedWriter ( new FileWriter (new_file) );
		
		while (scanner.hasNextLine()) {
			line = scanner.nextLine();
			if (! line.equals("</data-set>")) {
				writer.write(line + "\n");
			}
		}
		scanner.close();
		
		writer.write("<record>\n");
		writer.write("<description>" + description + "</description>\n");
		writer.write("<analysis>" + analysis + "</analysis>\n");
		writer.write("<location>" + location + "</location>\n");
		writer.write("<firstfault>" + fault + "</firstfault>\n");
		writer.write("<result>" + action + "</result>\n");
		writer.write("</record>\n");
		writer.write("</data-set>");
		
		writer.close();
		
		old_file.delete();
		new_file.renameTo(old_file);
	}
	
	public static void main(String[] args) throws OWLOntologyCreationException, IOException {}
}

